import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:minimal_shop/src/tools/tool_importer.dart';

class OrderStatusStyleObj {
  final Color color;
  final String text;
  OrderStatusStyleObj(this.color, this.text);
}

class TransactionStatusStyleObj {
  final Color color;
  final String text;
  final String status;
  TransactionStatusStyleObj(this.color, this.text, this.status);
}

get_order_status_style(text, order_type, old_driver_id, driver_id) {
  Color status_color;
  String status_text;

  switch (text) {
    case "DRIVERREQUEST":
      if (old_driver_id != null &&
          old_driver_id.toString() == Global.userId.toString()) {
        status_text = "Шилжүүлсэн /" + get_driver_name_by_id(driver_id) + "/";
        status_color = Colors.grey;
      } else {
        status_text = "Ирсэн /" + get_driver_name_by_id(old_driver_id) + "/";
        status_color = Colors.indigo;
      }
      break;
    case "WAITING":
      status_text = "Хүлээгдэж буй";
      status_color = Colors.blue;
      break;

    case "DELAYED":
      status_text = "Хойшилсон";
      status_color = Colors.yellow;
      break;
    case "DRIVERDISCARDED":
      //   return {"text": "Цуцлагдсан", "color": Colors.grey};
      status_text = "Жолооч цуцалсан";
      status_color = Colors.red;
      break;
    case "CUSTOMERDISCARDED":
      status_text = "Үйлчлүүлэгч цуцалсан";
      status_color = Colors.red;
      break;

    case "ITEMRETURN":
      status_text = "Буцаалт";
      status_color = Colors.grey;
      break;

    case "ITEMSWAP":
      status_text = "Бараа солих";
      status_color = Colors.grey;
      break;

    case "COMPLETED":
      status_text = "Хүргэлт хийгдсэн";
      status_color = Colors.green;
      break;
    default:
      status_text = "Шинэ хүргэлт";
      status_color = Colors.grey;
      break;
  }
  if (order_type == null) order_type = 0;
  if (order_type == 1) {
    status_text = status_text + " [Бараа солих] ";
  }
  if (order_type == 2) {
    status_text = status_text + " [Бараа буцаах] ";
  }

  return OrderStatusStyleObj(status_color, status_text);
}

get_transaction_type_style(text, status) {
  Color status_color;
  String type_text;
  String status_text;

  //  DELIVERY = 'DELIVERY'
  //   CUSTOMERRETURN = 'CUSTOMERRETURN'
  //   TRANSACTION = 'TRANSACTION'
  //   INTOWAREHOUSE = 'INTOWAREHOUSE'

  switch (text) {
    case "DELIVERY":
      type_text = "Хүргэлт";
      break;

    case "TRANSACTION":
      type_text = "Шилжүүлэг";
      break;

    case "CUSTOMERRETURN":
      type_text = "Үйлчлүүлэгч цуцлалт";
      break;

    case "INTOWAREHOUSE":
      type_text = "Агуулах руу буцаалт";
      break;

    default:
      type_text = "Шинэ хүргэлт";
      break;
  }
  switch (status) {
    case "approved":
      status_color = Colors.green;
      status_text = "хийгдсэн";
      break;

    case "requested":
      status_color = Colors.yellow;
      status_text = "хүсэлт илгээдсэн";
      break;

    case "canceled":
      status_text = "цуцалсан";
      status_color = Colors.grey;
      break;

    case "declined":
      status_text = "татгалзсан";
      status_color = Colors.red;
      break;

    default:
      status_text = "хийгдсэн";
      status_color = Colors.green;
      break;
  }

  return TransactionStatusStyleObj(status_color, type_text, status_text);
}

get_total_price(items, status) {
  if (status == 'COMPLETED') {
    return items.fold(
        0, (prev, item) => prev + item['price'] * item['delivered_count']);
  } else
    return items.fold(0, (prev, item) => prev + item['price'] * item['count']);
}

get_total_quantity(items, status) {
  if (status == 'COMPLETED')
    return items.fold(0, (prev, item) => prev + item['delivered_count']);
  else
    return items.fold(0, (prev, item) => prev + item['count']);
}

get_transaction_address(obj, type) {
  if (type == "from") {
    if (obj["type"] == "DELIVERY" || obj["type"] == "INTOWAREHOUSE") {
      return "Өөрөөс";
    }

    if (obj["type"] == "CUSTOMERRETURN") {
      return obj["customer"]["phone_number"];
    }

    if (obj["type"] == "TRANSACTION") {
      if (obj["from_warehouse"]["driver_id"] != null &&
          obj["from_warehouse"]["driver_id"] == Global.userId) return "Өөрөөс";
      return obj["from_warehouse"]["name"].toString();
    }
  }
  if (type == "to") {
    if (obj["type"] == "DELIVERY") {
      return obj["customer"]["phone_number"].toString();
    }
    if (obj["type"] == "INTOWAREHOUSE") {
      return "Агуулах";
    }
    if (obj["type"] == "TRANSACTION") {
      return obj["to_warehouse"]["name"].toString();
    }
    if (obj["type"] == "CUSTOMERRETURN") {
      return "Буцаалт";
    }
  }
}

get_driver_name_by_id(driver_id) {
  for (int i = 0; i < Global.warehouses.length; i += 1) {
    if (Global.warehouses[i]['driver_id'].toString() == driver_id.toString())
      return Global.warehouses[i]['name'];
  }
  return '';
}
