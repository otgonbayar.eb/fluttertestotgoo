import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MinimalToaster {
  static showToast(String message, {int len = 2}) {
    final toastLength = len == 1 ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG;

    Fluttertoast.showToast(
      msg: message,
      toastLength: toastLength,
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.grey[600],
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }
}
