class Global {
  // static String rootUrl = "http://139.177.191.21:5001";
  static String rootUrl = "http://18.179.243.6";
  // static String rootUrl = "http://localhost:5000";
  static String loginUrl = "${rootUrl}/api/login";
  static String usersUrl = "${rootUrl}/api/users";
  static String meUrl = "${rootUrl}/api/me";
  static String ordersUrl = "${rootUrl}/api/orders";
  static String itemsUrl = "${rootUrl}/api/items";
  static String transactionUrl = "${rootUrl}/api/transactions";
  static String warehouseUrl = "${rootUrl}/api/warehouses";
  static String itemsRemainderUrl = "${rootUrl}/api/warehouses/items_balance";
  static String closeWorkUrl = "${rootUrl}/api/drivers/close-work-day";
  static String checkWorkUrl = "${rootUrl}/api/driver/salary-tmp-status";
  static String ledgeUrl = "${rootUrl}/api/salary/driver/days";
  static String notificationUrl = "${rootUrl}/api/drivers/notifications";

  static String itemDeliveryUrl = "${rootUrl}/api/items/delivery_report";
  static String itemDeliveryOrderUrl =
      "${rootUrl}/api/items/delivery_report/orders";
  static String selcetedMoneyLog = "Бэлэн";
  static bool isLoggedIn = false;
  static String loginToken = "";
  static String userName = "";
  static int userId = 0;
  static int notifCount = 0;
  static int warehouseId = 0;
  static List orders = [];
  static List notifications = [];
  static List warehouses = [];

  static List items = [];
  static var selectedOrder = {};
  static List itemsToSave = [];
  static DateTime filterStartDate = DateTime.now();
  static DateTime filterendDate = DateTime.now();
  static List tmp_orders = [];
  static List tmp_item_orders = [];

  static String savedUsername = "";
  static String savedPassword = "";
  static bool saveCred = false;
}
