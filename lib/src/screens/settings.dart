// Copyright 2021, the Flutter project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:minimal_shop/src/tools/toast.dart';
import 'package:url_launcher/link.dart';
import 'package:http/http.dart' as http;
import '../auth.dart';
import '../routing.dart';
import '../tools/globals.dart';
import 'dart:convert';

import '../widgets/ledge_dialog.dart';
import '../widgets/money_log_dialog.dart';
import '../widgets/notif_dialog.dart';
import '../widgets/password_reset.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});
  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  late DateTime _startDate = DateTime.now();
  var salaryInfo = {};
  var moneyInfo = {};

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {
    try {
      Map<String, String> queryParams = {
        'date': '${_startDate.year}-${_startDate.month}-${_startDate.day}',
      };
      String queryString = Uri(queryParameters: queryParams).query;
      String url = Global.checkWorkUrl;

      Map<String, String> headers = {
        'Authorization': 'Bearer ${Global.loginToken}'
      };
      var response =
          await http.get(Uri.parse('$url?$queryString'), headers: headers);
      if (response.statusCode == 200) {
        setState(() {
          salaryInfo = json.decode(response.body)['summary'];
          moneyInfo = calculateMoney(salaryInfo['orders']);
        });
      }
    } on Object catch (error, stackTrace) {
      // Handle request error
      print('Request error: $error');
      print('Stack trace: $stackTrace');
    }
  }

  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _startDate,
      firstDate: DateTime.now().subtract(Duration(days: 1)),
      lastDate: DateTime.now(),
    );

    if (picked != null && picked != _startDate) {
      setState(() {
        _startDate = picked;
      });
      fetchData();
    }
  }

  void closeWork() async {
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${Global.loginToken}'
    };
    Map<String, String> queryParams = {
      'date': '${_startDate.year}-${_startDate.month}-${_startDate.day}',
    };
    String queryString = Uri(queryParameters: queryParams).query;

    var response = await http.get(
        Uri.parse('${Global.closeWorkUrl}?$queryString'),
        headers: headers);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      MinimalToaster.showToast("Амжилттай");
    } else {
      String job_status = json.decode(response.body)['status'];

      if (job_status == "not_delivered") {
        MinimalToaster.showToast(
            "Танд хүргээгүй хүргэлт байна жагсаалтаа шалгана уу! ");
      }
      if (job_status == "not_approved") {
        MinimalToaster.showToast(
            "Таны шилжүүлсэн хүргэлтийг хүлээж аваагүй байна. Жагсаалтаа шалгана уу! ");
      }

      Map<String, String> notifParam = {
        'date':
            '${DateTime.now().year}-${DateTime.now().month}-${DateTime.now().day}',
      };
      String qryStringNotif = Uri(queryParameters: notifParam).query;

      var response_notif = await http.get(
          Uri.parse('${Global.notificationUrl}?${qryStringNotif}'),
          headers: headers);

      if (response_notif.statusCode == 200) {
        var notifs = [];
        notifs = json.decode(response_notif.body)['notifications'];
        Global.notifications = notifs;
      }

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            insetPadding: EdgeInsets.zero,
            child: FractionallySizedBox(
              widthFactor: 0.85,
              alignment: Alignment.centerRight,
              child: NotifLogDialog(
                callback: fetchData,
              ),
            ),
          );
        },
      );
    }
  }

  calculateMoney(orders) {
    var ret_obj = {"totalCash": 0, "totalRemit": 0, "totalOwe": 0};
    int totalCash = 0;
    int totalRemit = 0;
    int totalOwe = 0;

    for (var obj in orders) {
      if (obj['driver_note'] != null && obj['driver_note'] != "") {
        Map<String, dynamic> driverNote = jsonDecode(obj['driver_note']);
        if (driverNote['cash'] != "") {
          totalCash += int.parse(driverNote['cash']);
        }
        if (driverNote['remit'] != "") {
          totalRemit += int.parse(driverNote['remit']);
        }
        if (driverNote['owe'] != "") {
          totalOwe += int.parse(driverNote['owe']);
        }
      }
    }
    ret_obj["totalCash"] = totalCash;
    ret_obj["totalRemit"] = totalRemit;
    ret_obj["totalOwe"] = totalOwe;
    return ret_obj;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(children: [
              ...[
                Text(
                  'Тохиргоо',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                ElevatedButton(
                  onPressed: () => _selectStartDate(context),
                  child: Text(
                      'Ажлын өдөр: ${_startDate.toString().split(' ')[0]}'),
                ),
                Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.all(8.0), // Adjust the spacing as needed
                      child: NumberWithDescription(
                          salaryInfo["per_delivery_price"] ?? 0,
                          'Нэгж хүргэлтийн цалин',
                          true),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.all(8.0), // Adjust the spacing as needed
                      child: NumberWithDescription(
                          salaryInfo["salary_amount"] ?? 0, 'Нийт цалин', true),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.all(8.0), // Adjust the spacing as needed
                      child: NumberWithDescription(
                          salaryInfo["invoice_amount"] ?? 0,
                          'Нийт орлого',
                          true),
                    ),
                  ],
                )),
                Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Global.tmp_orders = salaryInfo['orders'];
                        Global.selcetedMoneyLog = "Бэлэн";
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              insetPadding: EdgeInsets.zero,
                              child: MoneyLogDialog(
                                callback: fetchData,
                              ),
                            );
                          },
                        );
                      },
                      child: Padding(
                        padding:
                            EdgeInsets.all(8.0), // Adjust the spacing as needed
                        child: NumberWithDescription(
                            moneyInfo["totalCash"] ?? 0, 'Бэлэн', true),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Global.tmp_orders = salaryInfo['orders'];
                        Global.selcetedMoneyLog = "Данс";
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              insetPadding: EdgeInsets.zero,
                              child: MoneyLogDialog(
                                callback: fetchData,
                              ),
                            );
                          },
                        );
                      },
                      child: Padding(
                        padding:
                            EdgeInsets.all(8.0), // Adjust the spacing as needed
                        child: NumberWithDescription(
                            moneyInfo["totalRemit"] ?? 0, 'Данс', true),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Global.tmp_orders = salaryInfo['orders'];
                        Global.selcetedMoneyLog = "Авлага";
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              insetPadding: EdgeInsets.zero,
                              child: MoneyLogDialog(
                                callback: fetchData,
                              ),
                            );
                          },
                        );
                      },
                      child: Padding(
                        padding:
                            EdgeInsets.all(8.0), // Adjust the spacing as needed
                        child: NumberWithDescription(
                            moneyInfo["totalOwe"] ?? 0, 'Авлага', true),
                      ),
                    ),
                  ],
                ))
                // )
                ,
                Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.all(8.0), // Adjust the spacing as needed
                      child: NumberWithDescription(
                          salaryInfo["all_order_count"] ?? 0,
                          'Нийт захиалга',
                          false),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.all(8.0), // Adjust the spacing as needed
                      child: NumberWithDescription(
                          salaryInfo["customer_discard_count"] ?? 0,
                          'Үйлчлүүлэгч урьдчилан цуцалсан',
                          false),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.all(8.0), // Adjust the spacing as needed
                      child: NumberWithDescription(
                          salaryInfo["declined_order_count"] ?? 0,
                          'Жолооч цуцалсан',
                          false),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.all(8.0), // Adjust the spacing as needed
                      child: NumberWithDescription(
                          salaryInfo["delayed_order_count"] ?? 0,
                          'Хойшлуулсан',
                          false),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.all(8.0), // Adjust the spacing as needed
                      child: NumberWithDescription(
                          salaryInfo["delivered_order_count"] ?? 0,
                          'Хүргэсэн',
                          false),
                    ),
                  ],
                )),
                Container(
                  width: double.infinity,
                  height: 50,
                  color: Colors.blue,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.blue),
                    ),
                    onPressed: () async {
                      // call your async function here
                      closeWork();
                    },
                    child: Text('Ажлаа хаах'),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  color: Colors.blue,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.blue),
                    ),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            insetPadding: EdgeInsets.zero,
                            child: LedgeDialog(callback: fetchData),
                          );
                        },
                      );
                    },
                    child: Text('Тайлан'),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  color: Colors.grey,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.yellow),
                    ),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            insetPadding: EdgeInsets.zero,
                            child: ChangePasswordDialog(),
                          );
                        },
                      );
                    },
                    child: Text('Нууц үг солих'),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  color: Colors.grey,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.red),
                    ),
                    onPressed: () async {
                      // call your async function here
                      MinimalAuthScope.of(context).signOut();
                    },
                    child: Text('Системээс гарах'),
                  ),
                ),
              ]
            ]),
          ),
        ),
      );
}

class NumberWithDescription extends StatelessWidget {
  final int number;
  final String description;
  final bool currency;
  const NumberWithDescription(this.number, this.description, this.currency);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (!currency)
          Text(
            number.toString(),
            style: TextStyle(
              fontSize: 25, // Adjust the font size as needed
              fontWeight: FontWeight.bold,
            ),
          ),
        if (currency)
          Text(
            NumberFormat('#,###').format(int.parse(number.toString())) + '₮',
            style: TextStyle(
              fontSize: 25, // Adjust the font size as needed
              fontWeight: FontWeight.bold,
            ),
          ),
        SizedBox(
            height: 8), // Adjust the spacing between the number and description
        Text(
          description,
          style: TextStyle(
            fontSize: 10, // Adjust the font size as needed
            fontWeight: FontWeight.normal,
          ),
        ),
      ],
    );
  }
}
