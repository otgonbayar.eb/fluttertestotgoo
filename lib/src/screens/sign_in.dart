// Copyright 2021, the Flutter project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../auth.dart';
import '../tools/globals.dart';
import '../tools/toast.dart';

class Credentials {
  final String username;
  final String password;

  Credentials(this.username, this.password);
}

class SignInScreen extends StatefulWidget {
  final ValueChanged<Credentials> onSignIn;

  const SignInScreen({
    required this.onSignIn,
    super.key,
  });

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final box = GetStorage();
  // Initialize secure storage

  bool _rememberMe = false;
  @override
  void initState() {
    super.initState();
    String save_cred = "";
    try {
      save_cred = box.read('save_cred');
    } catch (err) {}
    if (save_cred != "") _loadSavedCredentials();
  }

  void _loadSavedCredentials() async {
    String username = "";
    String password = "";

    try {
      username = box.read('username');
      password = box.read('password');
    } catch (err) {}
    if (username != "" && password != "") {
      setState(() {
        _usernameController.text = username;
        _passwordController.text = password;
        _rememberMe = true;
      });
    }
  }

  void _saveCredentials() async {
    if (_rememberMe) {
      box.write('username', _usernameController.text);
      box.write('password', _passwordController.text);
      box.write('save_cred', "true");
      Global.saveCred = true;
    } else {
      try {
        box.remove('username');
        box.remove('password');
        box.remove('save_cred');
      } catch (err) {}
      Global.savedUsername = "";
      Global.savedPassword = "";
    }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: Card(
            child: Container(
              constraints: BoxConstraints.loose(const Size(600, 600)),
              padding: const EdgeInsets.all(8),
              child: AutofillGroup(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text('Minimal Shop',
                      style: Theme.of(context).textTheme.headlineMedium),
                  TextField(
                    autofillHints: [AutofillHints.username],
                    decoration: const InputDecoration(labelText: 'Нэвтрэх нэр'),
                    controller: _usernameController,
                  ),
                  TextField(
                    autofillHints: [AutofillHints.password],
                    decoration: const InputDecoration(labelText: 'Нууц үг'),
                    obscureText: true,
                    controller: _passwordController,
                  ),
                  Row(
                    children: [
                      Checkbox(
                        value: _rememberMe,
                        onChanged: (value) {
                          setState(() {
                            _rememberMe = value!;
                          });
                        },
                      ),
                      Text('Хадгалах'),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: TextButton(
                      onPressed: () async {
                        try {
                          final response = await http.post(
                            Uri.parse(Global.loginUrl),
                            headers: {'Content-Type': 'application/json'},
                            body: jsonEncode({
                              'username': _usernameController.text,
                              'password': _passwordController.text
                            }),
                          );
                          if (response.statusCode == 200) {
                            final login_info = json.decode(response.body);
                            if (login_info["access_token"] != "") {
                              Global.isLoggedIn = true;
                              Global.loginToken = login_info["access_token"];
                              String tt = login_info["access_token"];
                              Map<String, String> headers = {
                                'Authorization': 'Bearer $tt'
                              };
                              http.Response response_me = await http.get(
                                  Uri.parse(Global.meUrl),
                                  headers: headers);
                              final me_info = json.decode(response_me.body);
                              Global.userName = me_info["user"]["name"];
                              Global.userId = me_info["user"]["id"];
                              Global.warehouseId = me_info["warehouse"]["id"];
                              widget.onSignIn(Credentials(
                                  _usernameController.value.text,
                                  _passwordController.value.text));
                            } else {
                              MinimalToaster.showToast(
                                  "Оруулсан мэдээлэл буруу байна.");
                            }
                            _saveCredentials();
                          } else {
                            MinimalToaster.showToast(
                                "Оруулсан мэдээлэл буруу байна.");
                          }
                        } on Object catch (error, stackTrace) {
                          MinimalToaster.showToast("Алдаа гарлаа");
                          print('Request error: $error');
                          print('Stack trace: $stackTrace');
                        }
                      },
                      child: const Text('Нэвтрэх'),
                    ),
                  ),
                ],
              )),
            ),
          ),
        ),
      );
}
