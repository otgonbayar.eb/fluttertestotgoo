import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/material.dart';
import '../routing.dart';
import '../tools/tool_importer.dart';
import '../widgets/custom_appbar.dart';
import '../widgets/item_select_dialog.dart';

class NewOrderDetailsScreen extends StatefulWidget {
  const NewOrderDetailsScreen({
    super.key,
  });

  @override
  State<NewOrderDetailsScreen> createState() => _NewOrdersScreenState();
}

class _NewOrdersScreenState extends State<NewOrderDetailsScreen>
    with SingleTickerProviderStateMixin {
  var selectedItems = [];
  List allItems = [];
  final _customerPhoneNumberController = TextEditingController();
  final _customerAddressController = TextEditingController();
  final _cashController = TextEditingController();
  final _remitController = TextEditingController();
  final _oweController = TextEditingController();

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  void fetchData() async {}

  void updateValue() {
    setState(() {});
  }

  RouteState get _routeState => RouteStateScope.of(context);

  void saveOrder(context) async {
    Global.itemsToSave = [];
    Global.itemsToSave = [];
    for (final selectedItem in Global.selectedOrder["items"]) {
      Global.itemsToSave.add({
        "item_id": selectedItem["item_id"],
        "count": selectedItem["count"],
        "name": selectedItem["item"]["name"],
        "price": selectedItem["price"],
      });
    }
    if (Global.itemsToSave.length > 0) {
      String url = '${Global.ordersUrl}/create_by_driver';
      Map<String, String> headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${Global.loginToken}'
      };
      try {
        final response = await http.post(
          Uri.parse(url),
          headers: headers,
          body: jsonEncode({
            "customer_phone_number":
                _customerPhoneNumberController.text.toString(),
            "customer_address": _customerAddressController.text.toString(),
            "operator_id": Global.userId.toString(),
            "driver_id": Global.userId.toString(),
            "items": Global.itemsToSave,
            "note": {
              "cash": _cashController.text.toString(),
              "remit": _remitController.text.toString(),
              "owe": _oweController.text.toString()
            }
          }),
        );
        if (response.statusCode >= 200 && response.statusCode < 300) {
          MinimalToaster.showToast("Амжилттай");
          _routeState.go('/orders/all');
        } else
          MinimalToaster.showToast(response.body);
      } on Object catch (error, stackTrace) {
        MinimalToaster.showToast("Алдаа гарлаа");
        print('Request error: $error');
        print('Stack trace: $stackTrace');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var total_price = Global.selectedOrder['items']
        .fold(0, (prev, item) => prev + item['price'] * item['count']);

    return Scaffold(
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
          child: Center(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 50,
              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: ElevatedButton(
                onPressed: () {
                  showItemSelectDialog(context, updateValue);
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.blue),
                ),
                child: Text('Бараа нэмэх'),
              ),
            ),
            Container(
                height: 350,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: DataTable(
                    columns: const [
                      DataColumn(label: Text('Барааны нэр')),
                      DataColumn(label: Text('Тоо ширхэг')),
                      DataColumn(label: Text('Нэгжийэ үнэ')),
                    ],
                    rows: Global.selectedOrder['items'].map<DataRow>((item) {
                      return DataRow(cells: [
                        DataCell(Text(item['item']['name'])),
                        // DataCell(Text('${item['count'].toString()}ш')),
                        DataCell(
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              IconButton(
                                icon: const Icon(Icons.remove),
                                onPressed: () {
                                  setState(() {
                                    item['count']--;
                                  });
                                },
                              ),
                              Text(item['count'].toString()),
                              IconButton(
                                icon: const Icon(Icons.add),
                                onPressed: () {
                                  setState(() {
                                    item['count']++;
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                        DataCell(Text(NumberFormat('#,###')
                                .format(int.parse(item['price'].toString())) +
                            '₮'))
                      ]);
                    }).toList(),
                  ),
                )),
            Text(
              'Нийт үнэ: ${NumberFormat('#,###').format(int.parse(total_price.toString()))}₮',
              textAlign: TextAlign.center,
            ),
            TextFormField(
              maxLength: 8,
              inputFormatters: [
                FilteringTextInputFormatter.digitsOnly,
              ],
              maxLines: null,
              decoration: InputDecoration(
                labelText: 'Утасны дугаар',
                hintText: 'Утасны дугаар',
                border: OutlineInputBorder(),
              ),
              controller: _customerPhoneNumberController,
            ),
            TextFormField(
              maxLines: null,
              decoration: InputDecoration(
                labelText: 'Гэрийн хаяг',
                hintText: 'Хаяг',
                border: OutlineInputBorder(),
              ),
              controller: _customerAddressController,
            ),
            Container(
                width: double.infinity,
                height: 50,
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: TextFormField(
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  maxLines: null,
                  decoration: InputDecoration(
                    labelText: 'Бэлнээр',
                    hintText: 'Бэлнээр',
                    border: OutlineInputBorder(),
                  ),
                  controller: _cashController,
                )),
            Container(
              width: double.infinity,
              height: 50,
              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: TextFormField(
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                ],
                maxLines: null,
                decoration: InputDecoration(
                  labelText: 'Дансаар',
                  hintText: 'Дансаар',
                  border: OutlineInputBorder(),
                ),
                controller: _remitController,
              ),
            ),
            Container(
              width: double.infinity,
              height: 50,
              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: TextFormField(
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                ],
                maxLines: null,
                decoration: InputDecoration(
                  labelText: 'Авлага',
                  hintText: 'Авлага',
                  border: OutlineInputBorder(),
                ),
                controller: _oweController,
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    onPressed: () => {saveOrder(context)},
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.green),
                    ),
                    child: Text('Хадгалах'),
                  ),
                ),
              ],
            )
          ],
        ),
      )),
    );
  }
}
