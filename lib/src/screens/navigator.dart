// Copyright 2021, the Flutter project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

import '../auth.dart';
import '../data.dart';
import '../data/item.dart';
import '../routing.dart';
import '../screens/sign_in.dart';
import '../tools/globals.dart';
import '../widgets/fade_transition_page.dart';
import 'author_details.dart';
import 'order_details.dart';
import 'order_details.dart';
import 'new_order_detail.dart';
import 'scaffold.dart';

/// Builds the top-level navigator for the app. The pages to display are based
/// on the `routeState` that was parsed by the TemplateRouteParser.
class MinimalNavigator extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;

  const MinimalNavigator({
    required this.navigatorKey,
    super.key,
  });

  @override
  State<MinimalNavigator> createState() => _MinimalNavigatorState();
}

class _MinimalNavigatorState extends State<MinimalNavigator> {
  final _signInKey = const ValueKey('Sign in');
  final _scaffoldKey = const ValueKey('App scaffold');
  final _OrderDetailsKey = const ValueKey('Order details screen');
  final _authorDetailsKey = const ValueKey('Author details screen');

  @override
  Widget build(BuildContext context) {
    final routeState = RouteStateScope.of(context);
    final authState = MinimalAuthScope.of(context);
    final pathTemplate = routeState.route.pathTemplate;

    var selectedOrder;
    if (pathTemplate == '/order/:orderId') {
      var tmp = Global.orders.firstWhereOrNull(
          (o) => o["id"].toString() == routeState.route.parameters['orderId']);

      if (routeState.route.parameters['orderId'].toString() == 0.toString()) {
        selectedOrder = {
          "id": 0,
          "customer_phone_numer": "",
          "driver_id": Global.userId,
          "operator_id": null,
          "items": [],
          "status": "NEW"
        };
      } else
        selectedOrder = tmp;

      Global.selectedOrder = selectedOrder;
    }
    // Global.orderObj = selectedOrder!;
    Author? selectedAuthor;
    if (pathTemplate == '/author/:authorId') {
      selectedAuthor = libraryInstance.allAuthors.firstWhereOrNull(
          (b) => b.id.toString() == routeState.route.parameters['authorId']);
    }

    return Navigator(
      key: widget.navigatorKey,
      onPopPage: (route, dynamic result) {
        // When a page that is stacked on top of the scaffold is popped, display
        // the /Orders or /authors tab in MinimalScaffold.
        if (route.settings is Page &&
            (route.settings as Page).key == _OrderDetailsKey) {
          routeState.go('/orders/popular');
        }

        if (route.settings is Page &&
            (route.settings as Page).key == _authorDetailsKey) {
          routeState.go('/authors');
        }

        return route.didPop(result);
      },
      pages: [
        if (routeState.route.pathTemplate == '/signin')
          // Display the sign in screen.
          FadeTransitionPage<void>(
            key: _signInKey,
            child: SignInScreen(
              onSignIn: (credentials) async {
                var signedIn = await authState.signIn(
                    credentials.username, credentials.password);
                if (signedIn) {
                  await routeState.go('/orders/all');
                }
              },
            ),
          )
        else ...[
          // Display the app
          FadeTransitionPage<void>(
            key: _scaffoldKey,
            child: const MinimalScaffold(),
          ),
          // Add an additional page to the stack if the user is viewing a Order
          // or an author
          if (selectedOrder != null)
            if (selectedOrder["id"].toString() == "0")
              MaterialPage<void>(
                key: _OrderDetailsKey,
                child: NewOrderDetailsScreen(),
              )
            else
              MaterialPage<void>(
                key: _OrderDetailsKey,
                child: OrderDetailsScreen(),
              )
          else if (selectedAuthor != null)
            MaterialPage<void>(
              key: _authorDetailsKey,
              child: AuthorDetailsScreen(
                author: selectedAuthor,
              ),
            ),
        ],
      ],
    );
  }
}
