// Copyright 2021, the Flutter project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

import 'dart:convert';
import 'package:flutter/material.dart';
import '../routing.dart';
import '../tools/functions.dart';
import '../tools/globals.dart';
import '../tools/toast.dart';
import '../widgets/custom_appbar.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../widgets/transaction_maker.dart';

class TransactionsScreen extends StatefulWidget {
  const TransactionsScreen({
    super.key,
  });

  @override
  State<TransactionsScreen> createState() => _TransactionsScreenState();
}

class _TransactionsScreenState extends State<TransactionsScreen>
    with SingleTickerProviderStateMixin {
  TextEditingController _filterController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  late DateTime _startDate = DateTime.now();
  late DateTime _endDate = DateTime.now();
  List _filteredData = [];
  List _allData = [];
  int _pageNumber = 1;
  int pages = 1;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    fetchData();
    _scrollController.addListener(_scrollListener);
  }

  void _scrollListener() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (_pageNumber < pages) {
        _pageNumber++;
        fetchData();
      }
    }
  }

  @override
  void dispose() {
    _filterController.dispose();
    super.dispose();
  }

  Future<void> fetchData() async {
    setState(() {
      _isLoading = true;
    });

    try {
      Map<String, String> queryParams = {
        'start': '${_startDate.year}-${_startDate.month}-${_startDate.day}',
        'end': '${_endDate.year}-${_endDate.month}-${_endDate.day}',
        'page': _pageNumber.toString(),
      };

      String url = Global.transactionUrl;

      Map<String, String> headers = {
        'Authorization': 'Bearer ${Global.loginToken}'
      };
      String queryString = Uri(queryParameters: queryParams).query;
      var response =
          await http.get(Uri.parse('$url?$queryString'), headers: headers);
      if (response.statusCode == 200) {
        var tractions = json.decode(response.body)['transactions'];
        pages = json.decode(response.body)['pages'];
        var tmp = [];

        tractions.forEach((traction) {
          traction["dtls"].forEach((dtl) {
            tmp.add({
              "from_warehouse": traction["from_warehouse"],
              "to_warehouse": traction["to_warehouse"],
              "type": traction["type"],
              "status": traction["status"],
              "count": dtl["count"],
              "id": traction["id"],
              "item": dtl["item"],
              "price": dtl["price"],
              "customer": traction["customer"],
              "created_at": traction["created_at"],
              "updated_at": traction["updated_at"],
            });
          });
        });
        if (_pageNumber == 1)
          setState(() {
            _allData = tmp;
            _filteredData = tmp;
          });
        else {
          setState(() {
            _allData.addAll(tmp);
            _filteredData = _allData;
          });
        }
      }
    } on Object catch (error, stackTrace) {
      // Handle request error
      print('Request error: $error');
      print('Stack trace: $stackTrace');
    }
  }

  void updateValue() {
    setState(() {
      fetchData();
    });
  }

  void filterData(String filter) {
    setState(() {
      _filteredData = _allData.where((data) {
        final from_warehouse = data['from_warehouse']['name']!.toLowerCase();
        final to_warehouse = data['to_warehouse']['name']!.toLowerCase();
        final item_name = data['item']['name']!.toLowerCase();

        final searchTerm = filter.toLowerCase();
        return from_warehouse.contains(searchTerm) ||
            item_name.contains(searchTerm) ||
            to_warehouse.contains(searchTerm);
      }).toList();
    });
  }

  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _startDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _startDate) {
      setState(() {
        _startDate = picked;
      });
    }
  }

  Future<void> _selectEndDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _endDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _endDate) {
      setState(() {
        _endDate = picked;
      });
    }
  }

  void processTransaction(transaction) async {
    if (transaction['type'] == 'TRANSACTION' &&
        transaction['status'] == 'requested' &&
        transaction['to_warehouse']['driver_id'] == Global.userId) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Confirm'),
            content: Text('Шилжүүлгийг хүлээн авах уу?'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text('Үгүй'),
              ),
              TextButton(
                onPressed: () => Navigator.pop(context, true),
                child: Text('Тийм'),
              ),
            ],
          );
        },
      ).then((value) async {
        String respo = "APPROVE";
        if (!value) respo = "DECLINE";
        String url =
            '${Global.transactionUrl}/${transaction['id'].toString()}/status';
        Map<String, String> headers = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${Global.loginToken}'
        };
        var response = await http.put(
          Uri.parse(url),
          headers: headers,
          body: jsonEncode({"response": respo}),
        );
        print(response.body);
        updateValue();
      });
    }
    if (transaction['type'] == 'TRANSACTION' &&
        transaction['status'] == 'requested' &&
        transaction['from_warehouse']['driver_id'] == Global.userId) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Confirm'),
            content: Text('Шилжүүлгийг цуцлах уу?'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text('Үгүй'),
              ),
              TextButton(
                onPressed: () => Navigator.pop(context, true),
                child: Text('Тийм'),
              ),
            ],
          );
        },
      ).then((value) async {
        String respo = "CANCEL";
        if (value) {
          String url =
              '${Global.transactionUrl}/${transaction['id'].toString()}/status';
          Map<String, String> headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ${Global.loginToken}'
          };
          var response = await http.put(
            Uri.parse(url),
            headers: headers,
            body: jsonEncode({"response": respo}),
          );
          updateValue();
        }
      });
    }
  }

  void loadNextPage() {
    if (!_isLoading) {
      setState(() {
        _isLoading = true;
      });

      _pageNumber++; // Increment page number

      fetchData().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            controller: _filterController,
                            onChanged: filterData,
                            decoration: InputDecoration(
                              labelText: 'Хайх',
                            ),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _pageNumber = 1;
                            });
                            fetchData();
                          },
                          child: Text('Шүүх'),
                        ),
                      ],
                    ),
                    SizedBox(height: 8),
                    Row(
                      children: [
                        ElevatedButton(
                          onPressed: () => _selectStartDate(context),
                          child: Text(
                              'Эхлэх: ${_startDate.toString().split(' ')[0]}'),
                        ),
                        SizedBox(width: 8),
                        ElevatedButton(
                          onPressed: () => _selectEndDate(context),
                          child: Text(
                              'Дуусах: ${_endDate.toString().split(' ')[0]}'),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Expanded(
            child: Container(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                controller: _scrollController,
                child: Column(
                  children: [
                    SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: DataTable(
                          columns: const [
                            DataColumn(label: Text('Төрөл')),
                            DataColumn(label: Text('Хаанаас')),
                            DataColumn(label: Text('Хаашаа')),
                            DataColumn(label: Text('Барааны нэр')),
                            DataColumn(label: Text('Тоо')),
                            // DataColumn(label: Text('Үнэ')),
                            DataColumn(label: Text('Үүсгэсэн хугацаа')),
                            // DataColumn(label: Text('Зөвшөөрсөн хугацаа')),
                          ],
                          rows: _filteredData.map<DataRow>((dtl) {
                            return DataRow(cells: [
                              DataCell(GestureDetector(
                                  onDoubleTap: () {
                                    processTransaction(dtl);
                                  },
                                  child: Tooltip(
                                      message: get_transaction_type_style(
                                              dtl['type'], dtl['status'])
                                          .status,
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 4, horizontal: 8),
                                        decoration: BoxDecoration(
                                          color: get_transaction_type_style(
                                                  dtl['type'], dtl['status'])
                                              .color,
                                          borderRadius:
                                              BorderRadius.circular(4),
                                        ),
                                        child: Text(
                                          get_transaction_type_style(
                                                  dtl['type'], dtl['status'])
                                              .text,
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      )))),
                              DataCell(
                                  Text(get_transaction_address(dtl, "from"))),
                              DataCell(
                                  Text(get_transaction_address(dtl, "to"))),
                              DataCell(Text(dtl['item']['name'])),
                              DataCell(Text(dtl['count'].toString())),
                              // DataCell(Text(dtl['price'].toString())),
                              DataCell(Text(dtl['created_at'].toString())),
                              // DataCell(Text(dtl['updated_at'].toString())),
                            ]);
                          }).toList(),
                        ))
                  ],
                ),
              ),
            ),
          ),
          FloatingActionButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return Dialog.fullscreen(
                    child: TransactionDialog(callback: updateValue),
                  );
                },
              );
            },
            tooltip: 'Шинэ захиалга',
            child: const Icon(Icons.add),
          ),
        ],
      ),
    );
  }

  void createNewTransaction(BuildContext context) {}
}
