// Copyright 2021, the Flutter project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:minimal_shop/src/widgets/custom_appbar.dart';
import '../routing.dart';
import '../tools/tool_importer.dart';
import '../widgets/order_card.dart';
import 'dart:async';
import 'package:intl/intl.dart';

class OrdersScreen extends StatefulWidget {
  const OrdersScreen({
    super.key,
  });

  @override
  State<OrdersScreen> createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen>
    with SingleTickerProviderStateMixin {
  TextEditingController _filterController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  late DateTime _startDate = DateTime.now();
  late DateTime _endDate = DateTime.now();
  List _filteredData = [];
  List _allData = [];
  int _pageNumber = 1;
  int pages = 1;

  Timer? _timer;
  int itemsPerPage = 100; // Default value
  String selectedStatus = "Бүгд"; // Default value
  List<int> perPageOptions = [10, 20, 30, 50, 100];

  List<String> status_filter = [
    "",
    "DRIVERREQUEST",
    "WAITING",
    "DELAYED",
    "DRIVERDISCARDED",
    "CUSTOMERDISCARDED",
    "ITEMRETURN",
    "ITEMSWAP",
    "COMPLETED"
  ];
  List<String> status_text = [
    "Бүгд",
    "Шинэ хүргэлт",
    "Хүлээгдэж буй",
    "Хойшилсон",
    "Жолооч цуцалсан",
    "Үйлчлүүлэгч цуцалсан",
    "Буцаалт",
    "Бараа солих",
    "Хүргэлт хийгдсэн"
  ];
  @override
  void initState() {
    super.initState();

    fetchData();
    _scrollController.addListener(_scrollListener);
    _startTimer();
  }

  void _scrollListener() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (_pageNumber < pages) {
        _pageNumber++;
        fetchData();
      }
    }
  }

  @override
  void dispose() {
    _filterController.dispose();
    _cancelTimer();
    super.dispose();
  }

  void _startTimer() {
    // Create a periodic timer that runs every 10 seconds
    _timer = Timer.periodic(Duration(seconds: 300), (timer) {
      // Call your function here
      fetchData();
    });
  }

  void _cancelTimer() {
    // Cancel the timer if it is active
    _timer?.cancel();
    _timer = null;
  }

  Future<void> fetchData() async {
    try {
      Map<String, String> queryParams = {
        'start': '${_startDate.year}-${_startDate.month}-${_startDate.day}',
        'end': '${_endDate.year}-${_endDate.month}-${_endDate.day}',
        'driver_id': Global.userId.toString(),
        'page': _pageNumber.toString(),
        'limit': itemsPerPage.toString(),
        'status': status_filter[status_text.indexOf(selectedStatus)]
      };

      String url = Global.ordersUrl;

      Map<String, String> headers = {
        'Authorization': 'Bearer ${Global.loginToken}'
      };
      var response_warehouse =
          await http.get(Uri.parse('${Global.warehouseUrl}'), headers: headers);

      if (response_warehouse.statusCode == 200) {
        var houses = json.decode(response_warehouse.body)['warehouses'];

        Global.warehouses = houses
            .where((house) =>
                house['driver_id'] != null &&
                house['driver_id'] > 0 &&
                house['driver_id'] != Global.userId)
            .toList();
        ;
      }
      Map<String, String> notifParam = {
        'date':
            '${DateTime.now().year}-${DateTime.now().month}-${DateTime.now().day}',
      };
      String qryStringNotif = Uri(queryParameters: notifParam).query;

      var response_notif = await http.get(
          Uri.parse('${Global.notificationUrl}?${qryStringNotif}'),
          headers: headers);

      if (response_notif.statusCode == 200) {
        var notifs = [];
        notifs = json.decode(response_notif.body)['notifications'];
        Global.notifications = notifs;
      }
      String queryString = Uri(queryParameters: queryParams).query;
      var response =
          await http.get(Uri.parse('$url?$queryString'), headers: headers);
      if (response.statusCode == 200) {
        var tmp_orders = json.decode(response.body)['orders'];
        var orders = [];
        for (int i = 0; i < tmp_orders.length; i += 1) {
          if ((tmp_orders[i]['status'] == 'WAITING' ||
                  tmp_orders[i]['status'] == 'COMPLETED' ||
                  tmp_orders[i]['status'] == 'ITEMSWAP' ||
                  tmp_orders[i]['status'] == 'ITEMRETURN') &&
              tmp_orders[i]['driver_id'].toString() != Global.userId.toString())
            continue;
          orders.add(tmp_orders[i]);
        }
        pages = json.decode(response.body)['pages'];
        Global.orders = orders;
        setState(() {
          if (_pageNumber == 1) {
            _allData = orders;
            _filteredData = orders;
          } else if (json.decode(response.body)['total'] > _allData.length) {
            Global.orders.addAll(orders);
            _allData.addAll(orders);
            _allData = _allData.toSet().toList();
            _filteredData = _allData;
          }
        });
      }
      if (response.statusCode == 202) {
        String job_status = json.decode(response.body)['status'];
        if (job_status == "not_finished") {
          final yesterday = DateTime.now().subtract(Duration(days: 1));
          final formattedDate = DateFormat('yyyy-MM-dd').format(yesterday);
          String msg =
              '${'Та $formattedDate өдрийн ажлаа хаасан тохиолдолд захиалгын жагсаалтаа харах боломжтой'}';

          MinimalToaster.showToast(msg);
        }
      }
    } on Object catch (error, stackTrace) {
      // Handle request error
      print('Request error: $error');
      print('Stack trace: $stackTrace');
    }
  }

  void filterData(String filter) {
    setState(() {
      _filteredData = _allData.where((data) {
        final phone = data['customer']['phone_number']!.toLowerCase();
        final address = data['customer']['address']!.toLowerCase();
        final item_names = data['items'].fold(
            " ",
            (prev, item) =>
                prev + " " + item["item"]['name'].toString().toLowerCase());
        final searchTerm = filter.toLowerCase();
        return phone.contains(searchTerm) ||
            address.contains(searchTerm) ||
            item_names.contains(searchTerm);
      }).toList();
    });
  }

  setDates(String type) {
    if (type == "today") {
      setState(() {
        _startDate = DateTime.now();
        _endDate = DateTime.now();
      });
    }
    if (type == "tommorow") {
      setState(() {
        _startDate = DateTime.now().add(Duration(days: 1));
        ;
        _endDate = DateTime.now().add(Duration(days: 1));
        ;
      });
    }
    fetchData();
  }

  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _startDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _startDate) {
      setState(() {
        _startDate = picked;
      });
    }
  }

  Future<void> _selectEndDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _endDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _endDate) {
      setState(() {
        _endDate = picked;
      });
    }
  }

  void loadNextPage() {
    _pageNumber++; // Increment page number
    fetchData().then((_) {});
  }

  RouteState get _routeState => RouteStateScope.of(context);

  void processOrder(order) async {
    if (order['status'] == 'DRIVERREQUEST' &&
        order['driver_id'].toString() == Global.userId.toString()) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Confirm'),
            content: Text('Захиалга хүлээн авах эсэхийг сонгоно уу!'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text('Үгүй'),
              ),
              TextButton(
                onPressed: () => Navigator.pop(context, true),
                child: Text('Тийм'),
              ),
            ],
          );
        },
      ).then((value) async {
        String respo = "APPROVE";
        if (!value) respo = "CANCEL";
        String url = '${Global.ordersUrl}/${order['id'].toString()}/response';
        Map<String, String> headers = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${Global.loginToken}'
        };
        final response = await http.put(
          Uri.parse(url),
          headers: headers,
          body: jsonEncode({"response": respo}),
        );
        if (response.statusCode >= 200 && response.statusCode < 300) {
          fetchData();
          order['status'] = "WAITING";
          Global.selectedOrder = order;
          _routeState.go('/order/${order["id"]}');
        }
      });
    } else {
      Global.selectedOrder = order;
      _routeState.go('/order/${order["id"]}');
    }
  }

  void updateValue() {
    setState(() {
      Global.itemsToSave = Global.selectedOrder["items"];
      fetchData();
    });
  }

  createNewOrder() {
    Global.selectedOrder = {
      "id": "0",
      "customer_phone_numer": "",
      "driver_id": Global.userId,
      "operator_id": null,
      "items": [],
      "status": "NEW"
    };

    _routeState.go('/order/0');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            controller: _filterController,
                            onChanged: filterData,
                            decoration: InputDecoration(
                              labelText: 'хайх',
                            ),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            // Handle submit button press
                            // Fetch data from API based on selected date range and filter
                            fetchData();
                          },
                          child: Text('Мэдээлэл татах'),
                        ),
                      ],
                    ),
                    SizedBox(height: 8),
                    Row(
                      children: [
                        ElevatedButton(
                          onPressed: () => _selectStartDate(context),
                          child: Text(
                              'Эхлэх огноо: ${_startDate.toString().split(' ')[0]}'),
                        ),
                        SizedBox(width: 8),
                        ElevatedButton(
                          onPressed: () => _selectEndDate(context),
                          child: Text(
                              'Дуусах огноо: ${_endDate.toString().split(' ')[0]}'),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        ElevatedButton(
                            onPressed: () => setDates("today"),
                            child: Text('Өнөөдөр')),
                        SizedBox(width: 8),
                        ElevatedButton(
                          onPressed: () => setDates("tommorow"),
                          child: Text('Маргааш'),
                        ),
                        Padding(
                          padding: EdgeInsets.all(16.0),
                          child: DropdownButton<String>(
                            value: selectedStatus,
                            onChanged: (String? newValue) {
                              setState(() {
                                selectedStatus = newValue!;
                                fetchData();
                              });
                            },
                            items: status_text.map((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value.toString()),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Expanded(
              child: ListView.builder(
                  controller: _scrollController,
                  itemCount: _filteredData.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                        onTap: () {
                          processOrder(_filteredData[index]);
                        },
                        child: getCard(
                            index, _filteredData[index], _filteredData.length));
                  })),
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                FloatingActionButton(
                  onPressed: () {
                    createNewOrder();
                  },
                  tooltip: 'Шинэ захиалга',
                  child: const Icon(Icons.add),
                ),
                DropdownButton<int>(
                  value: itemsPerPage,
                  onChanged: (int? newValue) {
                    setState(() {
                      itemsPerPage = newValue!;
                      fetchData();
                    });
                  },
                  items: perPageOptions.map((int value) {
                    return DropdownMenuItem<int>(
                      value: value,
                      child: Text(value.toString()),
                    );
                  }).toList(),
                ),
              ]),
        ],
      ),
    );
  }
}
