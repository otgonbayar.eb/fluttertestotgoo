// Copyright 2021, the Flutter project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

import 'package:adaptive_navigation/adaptive_navigation.dart';
import 'package:flutter/material.dart';

import '../routing.dart';
import 'scaffold_body.dart';

class MinimalScaffold extends StatelessWidget {
  const MinimalScaffold({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final routeState = RouteStateScope.of(context);
    final selectedIndex = _getSelectedIndex(routeState.route.pathTemplate);

    return Scaffold(
      body: AdaptiveNavigationScaffold(
        selectedIndex: selectedIndex,
        body: const MinimalScaffoldBody(),
        onDestinationSelected: (idx) {
          if (idx == 0) routeState.go('/orders');
          if (idx == 1) routeState.go('/transactions');
          if (idx == 2) routeState.go('/warehouse');
          if (idx == 3) routeState.go('/settings');
        },
        destinations: const [
          AdaptiveScaffoldDestination(
            title: 'Захиалга',
            icon: Icons.book,
          ),
          AdaptiveScaffoldDestination(
            title: 'Гүйлгээнүүд',
            icon: Icons.person,
          ),
          AdaptiveScaffoldDestination(
            title: 'Барааны үлдэгдэл',
            icon: Icons.person,
          ),
          AdaptiveScaffoldDestination(
            title: 'Тохиргоо',
            icon: Icons.settings,
          ),
        ],
      ),
    );
  }

  int _getSelectedIndex(String pathTemplate) {
    if (pathTemplate.startsWith('/orders')) return 0;
    if (pathTemplate == '/transactions') return 1;
    if (pathTemplate == '/warehouse') return 2;
    if (pathTemplate == '/settings') return 3;
    return 0;
  }
}
