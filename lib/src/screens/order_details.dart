import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/material.dart';
import '../routing.dart';
import '../tools/tool_importer.dart';
import '../widgets/custom_appbar.dart';
import '../widgets/item_select_dialog.dart';

class OrderDetailsScreen extends StatefulWidget {
  const OrderDetailsScreen({
    super.key,
  });

  @override
  State<OrderDetailsScreen> createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrderDetailsScreen>
    with SingleTickerProviderStateMixin {
  var selectedItems = [];
  List allItems = [];
  final _driverCommentController = TextEditingController();
  final _discardCommentController = TextEditingController();
  final _cashController = TextEditingController();
  final _remitController = TextEditingController();
  final _oweController = TextEditingController();

  bool cashShow = false;
  bool remitShow = false;
  bool oweShow = false;
  int remainder = 0;

  late DateTime _postponeDate = DateTime.now().add(Duration(days: 1));
  String _driverComment = '';
  List warehouses = [];
  List filtered_warehouses = [];
  String? selected_warehouse;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  void fetchData() async {
    _driverCommentController.text = Global.selectedOrder["driver_comment"];

    String url = Global.warehouseUrl;
    Map<String, String> headers = {
      'Authorization': 'Bearer ${Global.loginToken}'
    };
    var response_warehouse =
        await http.get(Uri.parse('$url'), headers: headers);
    if (response_warehouse.statusCode == 200) {
      var houses = json.decode(response_warehouse.body)['warehouses'];

      setState(() {
        if (Global.selectedOrder['driver_note'] != null &&
            Global.selectedOrder['driver_note'] != "" &&
            Global.selectedOrder['driver_note'] != "null") {
          Map<String, dynamic> driverNote =
              jsonDecode(Global.selectedOrder['driver_note']);
          if (driverNote['cash'] != "") {
            _cashController.text = driverNote['cash'];
            cashShow = true;
          }
          if (driverNote['remit'] != "") {
            _remitController.text = driverNote['remit'];
            remitShow = true;
          }
          if (driverNote['owe'] != "") {
            _oweController.text = driverNote['owe'];
            oweShow = true;
          }
        }
        warehouses = houses
            .where((house) =>
                house['driver_id'] != null &&
                house['driver_id'] > 0 &&
                house['driver_id'] != Global.userId)
            .toList();
        ;
        filtered_warehouses = warehouses;
      });
    }
  }

  void updateValue() {
    setState(() {
      // Global.itemsToSave = Global.selectedOrder["items"];
    });
  }

  Future<void> _selectPostPoneDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _postponeDate,
      firstDate: DateTime.now().add(Duration(days: 1)),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _postponeDate) {
      setState(() {
        _postponeDate = picked;
      });
    }
  }

  RouteState get _routeState => RouteStateScope.of(context);

  void _showConfirmationDialog(type) async {
    String respo = "COMPLETED";
    Global.itemsToSave = [];
    if (Global.selectedOrder['status'] == 'COMPLETED' ||
        Global.selectedOrder['status'] == 'ITEMSWAP' ||
        Global.selectedOrder['status'] == 'ITEMRETURN') {
      respo = Global.selectedOrder['status'];
      for (final selectedItem in Global.selectedOrder["items"]) {
        Global.itemsToSave.add({
          "item_id": selectedItem["item_id"],
          "count": selectedItem["delivered_count"],
          "name": selectedItem["item"]["name"],
          "price": selectedItem["price"],
        });
      }
    } else
      for (final selectedItem in Global.selectedOrder["items"]) {
        Global.itemsToSave.add({
          "item_id": selectedItem["item_id"],
          "count": selectedItem["count"],
          "name": selectedItem["item"]["name"],
          "price": selectedItem["price"],
        });
      }
    if (Global.itemsToSave.length > 0) {
      String url =
          '${Global.ordersUrl}/${Global.selectedOrder["id"].toString()}/status';
      Map<String, String> headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${Global.loginToken}'
      };

      try {
        final response = await http.put(
          Uri.parse(url),
          headers: headers,
          body: jsonEncode({
            "status": respo,
            "driver_comment": _driverCommentController.text.toString(),
            "items": Global.itemsToSave,
            "note": {
              "cash": _cashController.text.toString(),
              "remit": _remitController.text.toString(),
              "owe": _oweController.text.toString()
            }
          }),
        );
        if (response.statusCode >= 200 && response.statusCode < 300) {
          MinimalToaster.showToast("Амжилттай");
          _routeState.go('/orders/all');
        } else
          MinimalToaster.showToast(response.body);
      } on Object catch (error, stackTrace) {
        MinimalToaster.showToast("Алдаа гарлаа");
        print('Request error: $error');
        print('Stack trace: $stackTrace');
      }
    } else
      MinimalToaster.showToast("Бараа заавал сонгон уу");
  }

  void _showDelayOrDiscardDialog(type) async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Дараах мэдээллийг заавал оруулна уу!'),
          content: Text(''),
          actions: <Widget>[
            //delay
            if (type == 1)
              ElevatedButton(
                onPressed: () => _selectPostPoneDate(context),
                child: Text(
                    'Дараа хүргэх огноо: ${_postponeDate.toString().split(' ')[0]}'),
              ),
            if (type == 2)
              TextFormField(
                maxLines: null,
                decoration: InputDecoration(
                  labelText: 'Цуцалсан шалтгаан',
                  border: OutlineInputBorder(),
                ),
                controller: _discardCommentController,
              ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: Text('Болсон'),
            ),
          ],
        );
      },
    ).then((value) async {
      if (value) {
        String respo = "DELAYED";
        switch (type) {
          case 1:
            respo = "DELAYED";
            break;
          case 2:
            respo = "DRIVERDISCARDED";
            break;
          default:
        }

        String url =
            '${Global.ordersUrl}/${Global.selectedOrder["id"].toString()}/status';
        Map<String, String> headers = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${Global.loginToken}'
        };
        if (type == 2) {
          if (_discardCommentController.text.toString().length == 0)
            MinimalToaster.showToast("Цуцалсан шатлгааныг оруулна уу!");
          else {
            try {
              final response = await http.put(
                Uri.parse(url),
                headers: headers,
                body: jsonEncode({
                  "status": respo,
                  "discard_comment": _discardCommentController.text.toString(),
                }),
              );
              if (response.statusCode >= 200 && response.statusCode < 300) {
                MinimalToaster.showToast("Амжилттай");
                _routeState.go('/orders/all');
              } else
                MinimalToaster.showToast(response.body);
            } on Object catch (error, stackTrace) {
              MinimalToaster.showToast("Алдаа гарлаа");
              print('Request error: $error');
              print('Stack trace: $stackTrace');
            }
          }
        }
        if (type == 1) {
          try {
            final response = await http.put(
              Uri.parse(url),
              headers: headers,
              body: jsonEncode({
                "status": respo,
                "delivery_date": _postponeDate.toString().split('.')[0],
              }),
            );
            if (response.statusCode >= 200 && response.statusCode < 300) {
              MinimalToaster.showToast("Амжилттай");
              _routeState.go('/orders/all');
            } else
              MinimalToaster.showToast(response.body);
          } on Object catch (error, stackTrace) {
            MinimalToaster.showToast("Алдаа гарлаа");
            print('Request error: $error');
            print('Stack trace: $stackTrace');
          }
        }
      }
    });
  }

  void _showCommentDialog() async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Дараах мэдээллийг заавал оруулна уу!'),
          content: Text(''),
          actions: <Widget>[
            ListTile(
              title: Text('Хаяг лавлах'),
              leading: Radio(
                value: 'Хаяг лавлах',
                groupValue: _driverCommentController,
                onChanged: (value) {
                  setState(() {
                    _driverComment = 'Хаяг лавлах';
                    _driverCommentController.text = _driverComment;
                  });
                },
              ),
            ),
            ListTile(
              title: Text('Утас лавлах'),
              leading: Radio(
                value: 'Утас лавлах',
                groupValue: _driverCommentController,
                onChanged: (value) {
                  setState(() {
                    _driverComment = 'Утас лавлах';
                    _driverCommentController.text = _driverComment;
                  });
                },
              ),
            ),
            ListTile(
              title: Text('Барааны нэр төрөл лавлах'),
              leading: Radio(
                value: 'Барааны нэр төрөл лавлах',
                groupValue: _driverCommentController,
                onChanged: (value) {
                  setState(() {
                    _driverComment = 'Барааны нэр төрөл лавлах';
                    _driverCommentController.text = _driverComment;
                  });
                },
              ),
            ),
            ListTile(
              title: Text('Захиалагчийн фэйсбүүк хаяг лавлах'),
              leading: Radio(
                value: 'Захиалагчийн фэйсбүүк хаяг лавлах',
                groupValue: _driverCommentController,
                onChanged: (value) {
                  setState(() {
                    _driverComment = 'Захиалагчийн фэйсбүүк хаяг лавлах';
                    _driverCommentController.text = _driverComment;
                  });
                },
              ),
            ),
            ListTile(
              title: Text('Захиалагчийн нэр лавлах'),
              leading: Radio(
                value: 'Захиалагчийн нэр лавлах',
                groupValue: _driverCommentController,
                onChanged: (value) {
                  setState(() {
                    _driverComment = 'Захиалагчийн нэр лавлах';
                    _driverCommentController.text = _driverComment;
                  });
                },
              ),
            ),
            TextFormField(
              maxLines: null,
              decoration: InputDecoration(
                labelText: 'Лавлагаа',
                border: OutlineInputBorder(),
              ),
              controller: _driverCommentController,
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: Text('Болсон'),
            ),
          ],
        );
      },
    ).then((value) async {
      if (value) {
        String url =
            '${Global.ordersUrl}/${Global.selectedOrder["id"].toString()}';
        Map<String, String> headers = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${Global.loginToken}'
        };

        try {
          final response = await http.put(
            Uri.parse(url),
            headers: headers,
            body: jsonEncode({
              "driver_comment": _driverCommentController.text,
            }),
          );
          if (response.statusCode >= 200 && response.statusCode < 300) {
            MinimalToaster.showToast("Амжилттай");
            _routeState.go('/orders/all');
          } else
            MinimalToaster.showToast(response.body);
        } on Object catch (error, stackTrace) {
          MinimalToaster.showToast("Алдаа гарлаа");
          print('Request error: $error');
          print('Stack trace: $stackTrace');
        }
      }
    });
  }

  void _driverChangeDialog() async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Дараах мэдээллийг заавал оруулна уу!'),
          content: Text(''),
          actions: <Widget>[
            DropdownButtonFormField<String>(
              value: selected_warehouse,
              onChanged: (value) {
                setState(() {
                  selected_warehouse = value;
                });
              },
              items: filtered_warehouses.map((option) {
                return DropdownMenuItem<String>(
                  value: option['driver_id'].toString(),
                  child: Text(option['name']),
                );
              }).toList(),
              decoration: InputDecoration(
                labelText: 'шилжүүлэх жолооч',
              ),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: Text('Болсон'),
            ),
          ],
        );
      },
    ).then((value) async {
      if (value) {
        String url =
            '${Global.ordersUrl}/${Global.selectedOrder["id"].toString()}/status';
        Map<String, String> headers = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${Global.loginToken}'
        };

        try {
          final response = await http.put(
            Uri.parse(url),
            headers: headers,
            body: jsonEncode({
              "driver_id": int.parse(selected_warehouse.toString()),
              "status": "DRIVERREQUEST",
            }),
          );
          if (response.statusCode >= 200 && response.statusCode < 300) {
            MinimalToaster.showToast("Амжилттай");
            _routeState.go('/orders/all');
          } else
            MinimalToaster.showToast(response.body);
        } on Object catch (error, stackTrace) {
          MinimalToaster.showToast("Алдаа гарлаа");
          print('Request error: $error');
          print('Stack trace: $stackTrace');
        }
      }
    });
  }

  void calculateAmount(type, total_price) {
    int cash = int.tryParse(_cashController.text) ?? 0;
    int remit = int.tryParse(_remitController.text) ?? 0;
    int owe = int.tryParse(_oweController.text) ?? 0;

    if (type == 'cash') {
      setState(() {
        cashShow = true;
        _cashController.text = (total_price - remit - owe).toString();
      });
    }
    if (type == 'remit') {
      setState(() {
        remitShow = true;
        _remitController.text = (total_price - cash - owe).toString();
      });
    }
    if (type == 'owe') {
      setState(() {
        oweShow = true;
        _oweController.text = (total_price - cash - remit).toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var total_price = 0;
    if (Global.selectedOrder['status'] == 'COMPLETED' ||
        Global.selectedOrder['status'] == 'ITEMSWAP' ||
        Global.selectedOrder['status'] == 'ITEMRETURN')
      total_price = Global.selectedOrder['items'].fold(
          0, (prev, item) => prev + item['price'] * item['delivered_count']);
    else
      total_price = Global.selectedOrder['items']
          .fold(0, (prev, item) => prev + item['price'] * item['count']);

    return Scaffold(
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
          child: Center(
        child: Column(
          children: [
            Row(
              children: [
                Text(Global.selectedOrder['customer']['phone_number']),
                InkWell(
                  child: Icon(Icons.call),
                  onTap: () async {
                    final Uri launchUri = Uri(
                        scheme: 'tel',
                        path:
                            '${Global.selectedOrder['customer']['phone_number']}');
                    await launchUrl(launchUri);
                  },
                ),
                InkWell(
                  child: Icon(Icons.sms),
                  onTap: () async {
                    final Uri launchUri = Uri(
                        scheme: 'sms',
                        path:
                            '${Global.selectedOrder['customer']['phone_number']}');
                    await launchUrl(launchUri);
                  },
                ),
              ],
            ),
            Text(
              Global.selectedOrder['customer']['address'],
              textAlign: TextAlign.left,
              maxLines: null,
            ),
            Container(
                height: 350,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: DataTable(
                    columns: const [
                      DataColumn(label: Text('Барааны нэр')),
                      DataColumn(label: Text('Тоо ширхэг')),
                      DataColumn(label: Text('Нэгжийн үнэ')),
                    ],
                    rows: Global.selectedOrder['items'].map<DataRow>((item) {
                      return DataRow(cells: [
                        DataCell(Text(item['item']['name'])),
                        // DataCell(Text('${item['count'].toString()}ш')),
                        if (Global.selectedOrder['status'] == 'COMPLETED' ||
                            Global.selectedOrder['status'] == 'ITEMSWAP' ||
                            Global.selectedOrder['status'] == 'ITEMRETURN')
                          DataCell(
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: const Icon(Icons.remove),
                                  onPressed: () {
                                    setState(() {
                                      item['delivered_count']--;
                                    });
                                  },
                                ),
                                Text(item['delivered_count'].toString()),
                                IconButton(
                                  icon: const Icon(Icons.add),
                                  onPressed: () {
                                    setState(() {
                                      item['delivered_count']++;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (Global.selectedOrder['status'] == 'WAITING' ||
                            Global.selectedOrder['status'] == 'DRIVERREQUEST' ||
                            Global.selectedOrder['status'] ==
                                'CUSTOMERDISCARDED')
                          DataCell(
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: const Icon(Icons.remove),
                                  onPressed: () {
                                    setState(() {
                                      item['count']--;
                                    });
                                  },
                                ),
                                Text(item['count'].toString()),
                                IconButton(
                                  icon: const Icon(Icons.add),
                                  onPressed: () {
                                    setState(() {
                                      item['count']++;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                        DataCell(Text(NumberFormat('#,###')
                                .format(int.parse(item['price'].toString())) +
                            '₮'))
                      ]);
                    }).toList(),
                  ),
                )),
            Row(
              mainAxisAlignment:
                  MainAxisAlignment.spaceEvenly, // Adjust alignment as needed
              children: [
                ElevatedButton(
                  onPressed: () {
                    calculateAmount('cash', total_price);
                  },
                  child: Text('Бэлэн'),
                ),
                ElevatedButton(
                  onPressed: () {
                    calculateAmount('remit', total_price);
                  },
                  child: Text('Данс'),
                ),
                ElevatedButton(
                  onPressed: () {
                    calculateAmount('owe', total_price);
                  },
                  child: Text('Авлага'),
                ),
              ],
            ),
            Text(
              'Нийт үнэ: ${NumberFormat('#,###').format(int.parse(total_price.toString()))}₮',
              textAlign: TextAlign.center,
            ),
            TextFormField(
              maxLines: null,
              decoration: InputDecoration(
                labelText: 'Тэмдэглэл',
                hintText: 'Нэмэлт тэмдэглэл',
                border: OutlineInputBorder(),
              ),
              controller: _driverCommentController,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (cashShow)
                  Container(
                      width: double.infinity,
                      height: 100,
                      child: TextFormField(
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                        ],
                        maxLines: null,
                        decoration: InputDecoration(
                          labelText: 'Бэлнээр',
                          hintText: 'Бэлнээр',
                          border: OutlineInputBorder(),
                        ),
                        controller: _cashController,
                      )),
                if (remitShow)
                  Container(
                    width: double.infinity,
                    height: 100,
                    child: TextFormField(
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                      maxLines: null,
                      decoration: InputDecoration(
                        labelText: 'Дансаар',
                        hintText: 'Дансаар',
                        border: OutlineInputBorder(),
                      ),
                      controller: _remitController,
                    ),
                  ),
                if (oweShow)
                  Container(
                    width: double.infinity,
                    height: 100,
                    child: TextFormField(
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                      maxLines: null,
                      decoration: InputDecoration(
                        labelText: 'Авлага',
                        hintText: 'Авлага',
                        border: OutlineInputBorder(),
                      ),
                      controller: _oweController,
                    ),
                  ),
                Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    onPressed: () => _showConfirmationDialog(1),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.green),
                    ),
                    child: Text('Хүргэлт хийгдсэн'),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      showItemSelectDialog(context, updateValue);
                    },
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.blue),
                    ),
                    child: Text('Бараа нэмэх'),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    onPressed: () => _showDelayOrDiscardDialog(1),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.yellow),
                    ),
                    child: Text('Хойшлуулах'),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    onPressed: () => _showDelayOrDiscardDialog(2),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.red),
                    ),
                    child: Text('Цуцлах'),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  color: Colors.grey,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.blueGrey),
                    ),
                    onPressed: () async {
                      // call your async function here
                      _driverChangeDialog();
                    },
                    child: Text('Шилжүүлэх'),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  color: Colors.grey,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.blueGrey),
                    ),
                    onPressed: () async {
                      // call your async function here
                      _showCommentDialog();
                    },
                    child: Text('Лавлах'),
                  ),
                ),
              ],
            ),
          ],
        ),
      )),
    );
  }
}
