// Copyright 2021, the Flutter project authors. Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

import 'dart:convert';
import 'package:flutter/material.dart';
import '../routing.dart';
import '../tools/functions.dart';
import '../tools/globals.dart';
import '../tools/toast.dart';
import '../widgets/custom_appbar.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../widgets/item_report_order.dart';
import '../widgets/transaction_maker.dart';

class WarehouseScreen extends StatefulWidget {
  const WarehouseScreen({
    super.key,
  });

  @override
  State<WarehouseScreen> createState() => _WarehouseScreenState();
}

class _WarehouseScreenState extends State<WarehouseScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  TextEditingController _filterController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  late DateTime _startDate = DateTime.now();
  late DateTime _endDate = DateTime.now();
  List _filteredData = [];
  List _allData = [];
  List _allItems = [];
  List _filteredItems = [];
  List _allRemainders = [];
  List _allDelivery = [];
  int _pageNumber = 1;
  int pages = 1;
  String? selected_item;
  String? selected_item1;
  bool _sortAscending = true;
  int _sortColumnIndex = 0;

  bool _sortAscending1 = true;
  int _sortColumnIndex1 = 0;

  @override
  void initState() {
    super.initState();
    fetchData();
    _scrollController.addListener(_scrollListener);
    _tabController = TabController(
        length: 3, vsync: this); // Replace 3 with the number of tabs you want
  }

  void _scrollListener() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (_pageNumber < pages) {
        _pageNumber++;
        fetchData();
      }
    }
  }

  @override
  void dispose() {
    _filterController.dispose();
    super.dispose();
  }

  Future<void> fetchData() async {
    try {
      Map<String, String> queryParams = {
        'date': '${_startDate.year}-${_startDate.month}-${_startDate.day}',
      };

      String url = Global.warehouseUrl;

      Map<String, String> headers = {
        'Authorization': 'Bearer ${Global.loginToken}'
      };
      String queryString = Uri(queryParameters: queryParams).query;
      var response = await http.get(
          Uri.parse('$url/${Global.warehouseId}?$queryString'),
          headers: headers);

      var item_response = await http
          .get(Uri.parse('${Global.itemsUrl}?limit=1000'), headers: headers);
      if (item_response.statusCode == 200) {
        var _items = json.decode(item_response.body)['items'];
        setState(() {
          _allItems = _items;
          _filteredItems = _allItems;
        });
      }
      print(Uri.parse('$url?$queryString'));
      if (response.statusCode == 200) {
        var items = json.decode(response.body)['items'];
        setState(() {
          _allData = items;
          _filteredData = items;
        });
      }
    } on Object catch (error, stackTrace) {
      print('Request error: $error');
      print('Stack trace: $stackTrace');
    }
  }

  void filterData(String filter) {
    setState(() {
      _filteredData = _allData.where((data) {
        final name = data['name']!.toLowerCase();
        final category_name = data['category_name']!.toLowerCase();
        final searchTerm = filter.toLowerCase();
        return name.contains(searchTerm) || category_name.contains(searchTerm);
      }).toList();
    });
  }

  void filterItem(String filter) {
    setState(() {
      _filteredItems = _allItems.where((data) {
        final name = data['name']!.toLowerCase();
        final searchTerm = filter.toLowerCase();
        return name.contains(searchTerm);
      }).toList();
    });
  }

  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _startDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _startDate) {
      setState(() {
        _startDate = picked;
      });
    }
  }

  Future<void> _selectEndDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _endDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _endDate) {
      setState(() {
        _endDate = picked;
      });
    }
  }

  fetch_other_item_remainder() async {
    String url = Global.itemsRemainderUrl;
    String? id = "0";
    if (selected_item != null) id = selected_item;
    Map<String, String> headers = {
      'Authorization': 'Bearer ${Global.loginToken}'
    };

    Map<String, String> queryParams = {
      'date': '${_startDate.year}-${_startDate.month}-${_startDate.day}',
      'item_id': id.toString()
    };
    String queryString = Uri(queryParameters: queryParams).query;
    var response =
        await http.get(Uri.parse('$url?$queryString'), headers: headers);

    var tmp_list = json.decode(response.body)['item_warehouse_balances'];
    var filtered = [];
    for (int i = 0; i < tmp_list.length; i++) {
      int pureRemainder = int.parse(tmp_list[i]["pure_remainder"].toString());
      int warehouse_id = int.parse(tmp_list[i]["warehouse"]['id'].toString());

      if (pureRemainder > 0 && warehouse_id > 5) {
        if (warehouse_id != 98) filtered.add(tmp_list[i]);
      }
      if (pureRemainder > 0 && warehouse_id == 1) {
        filtered.add(tmp_list[i]);
      }
    }

    setState(() {
      _allRemainders = filtered;
      _allRemainders.sort((a, b) {
        var pureRemainderA = a['pure_remainder'] ?? 0;
        var pureRemainderB = b['pure_remainder'] ?? 0;
        return pureRemainderA.compareTo(pureRemainderB);
      });
    });
  }

  fetch_item_delivery() async {
    String url = Global.itemDeliveryUrl;
    String? id = "0";
    if (selected_item1 != null) id = selected_item1;
    Map<String, String> headers = {
      'Authorization': 'Bearer ${Global.loginToken}'
    };

    Map<String, String> queryParams = {
      'start': '${_startDate.year}-${_startDate.month}-${_startDate.day}',
      'end': '${_endDate.year}-${_endDate.month}-${_endDate.day}',
    };
    String queryString = Uri(queryParameters: queryParams).query;
    var response = await http
        .get(Uri.parse('$url/${id.toString()}?$queryString'), headers: headers);

    var tmp_list = json.decode(response.body)['results'];

    DateFormat inputFormat = DateFormat("E, d MMM yyyy HH:mm:ss 'GMT'");
    DateFormat outputFormat = DateFormat("yyyy-MM-dd");
    tmp_list.forEach((item) {
      DateTime dateTime = inputFormat.parse(item["date"]);
      item["date"] = outputFormat.format(dateTime);
    });

    setState(() {
      _allDelivery = tmp_list;
    });
  }

  fetch_delivery_detail(String date) async {
    String url = Global.itemDeliveryOrderUrl;
    String? id = "0";
    if (selected_item1 != null) id = selected_item1;
    Map<String, String> headers = {
      'Authorization': 'Bearer ${Global.loginToken}'
    };

    Map<String, String> queryParams = {
      'date': date,
      'item_id': id.toString(),
    };
    String queryString = Uri(queryParameters: queryParams).query;
    var response =
        await http.get(Uri.parse('$url/?$queryString'), headers: headers);

    var tmp_list = json.decode(response.body)['results'];

    DateFormat inputFormat = DateFormat("E, d MMM yyyy HH:mm:ss 'GMT'");
    DateFormat outputFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    tmp_list.forEach((item) {
      DateTime dateTime = inputFormat.parse(item["delivered_date"]);
      item["delivered_date"] = outputFormat.format(dateTime);
    });
    Global.tmp_item_orders = tmp_list;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          insetPadding: EdgeInsets.zero,
          child: ItemReportDialog(
            callback: fetch_item_delivery,
          ),
        );
      },
    );
  }

  void _sort<T>(Comparable<T> Function(Map<String, dynamic>) getField) {
    _filteredData.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);
      return _sortAscending
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });
  }

  void _sort1<T>(Comparable<T> Function(Map<String, dynamic>) getField) {
    _allRemainders.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);
      return _sortAscending1
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize:
            Size.fromHeight(kToolbarHeight + 30), // Adjust the height as needed
        child: AppBar(
          bottom: PreferredSize(
            preferredSize:
                Size.fromHeight(20), // Adjust the height of the TabBar
            child: Align(
              alignment: Alignment.topCenter,
              child: TabBar(
                controller: _tabController,
                isScrollable: true,
                tabs: [
                  Tab(text: 'Өөрийн'),
                  Tab(text: 'Бараа хүргэлтийн мэдээлэл'),
                  Tab(text: 'Жолоочийн үлдэгдэл ')
                ],
              ),
            ),
          ),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          Center(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: TextField(
                                  controller: _filterController,
                                  onChanged: filterData,
                                  decoration: InputDecoration(
                                    labelText: 'Хайх',
                                  ),
                                ),
                              ),
                              ElevatedButton(
                                onPressed: () => _selectStartDate(context),
                                child: Text(
                                    'Өдөр сонгох: ${_startDate.toString().split(' ')[0]}'),
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    _pageNumber = 1;
                                  });
                                  fetchData();
                                },
                                child: Text('Шүүх'),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                        ],
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Container(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      controller: _scrollController,
                      child: Column(
                        children: [
                          SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: DataTable(
                                sortColumnIndex: _sortColumnIndex,
                                sortAscending: _sortAscending,
                                columns: [
                                  DataColumn(
                                    label: Text('Нэр'),
                                    onSort: (columnIndex, ascending) {
                                      setState(() {
                                        _sortColumnIndex = columnIndex;
                                        _sortAscending = ascending;
                                        _sort<String>((dtl) => dtl['name']);
                                      });
                                    },
                                  ),
                                  DataColumn(
                                    label: Text('Үнэ'),
                                    onSort: (columnIndex, ascending) {
                                      setState(() {
                                        _sortColumnIndex = columnIndex;
                                        _sortAscending = ascending;
                                        _sort<num>((dtl) => dtl['price']);
                                      });
                                    },
                                  ),
                                  DataColumn(
                                    label: Text('Тоо'),
                                    onSort: (columnIndex, ascending) {
                                      setState(() {
                                        _sortColumnIndex = columnIndex;
                                        _sortAscending = ascending;
                                        _sort<num>((dtl) =>
                                            int.tryParse(dtl['count']) ?? 0);
                                      });
                                    },
                                  ),
                                  DataColumn(
                                    label: Text('Цэвэр үлдэгдэл'),
                                    onSort: (columnIndex, ascending) {
                                      setState(() {
                                        _sortColumnIndex = columnIndex;
                                        _sortAscending = ascending;
                                        _sort<num>((dtl) =>
                                            int.tryParse(
                                                dtl['pure_remainder']) ??
                                            0);
                                      });
                                    },
                                  ),
                                ],
                                rows: _filteredData.map<DataRow>((dtl) {
                                  return DataRow(cells: [
                                    DataCell(Text(dtl['name'])),
                                    DataCell(Text(NumberFormat('#,###').format(
                                            int.parse(
                                                dtl['price'].toString())) +
                                        '₮')),
                                    DataCell(
                                        Text(dtl['count'] ?? 0.toString())),
                                    DataCell(Text(
                                        dtl['pure_remainder'] ?? 0.toString())),
                                  ]);
                                }).toList(),
                              )),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Center(
              child: Column(
            children: [
              Row(
                children: [
                  ElevatedButton(
                    onPressed: () => _selectStartDate(context),
                    child: Text(
                        'Эхлэх огноо: ${_startDate.toString().split(' ')[0]}'),
                  ),
                  SizedBox(width: 8),
                  ElevatedButton(
                    onPressed: () => _selectEndDate(context),
                    child: Text(
                        'Дуусах огноо: ${_endDate.toString().split(' ')[0]}'),
                  ),
                ],
              ),
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: DropdownButtonFormField<String>(
                    value: selected_item1,
                    onChanged: (value) {
                      setState(() {
                        selected_item1 = value;
                      });
                      fetch_item_delivery();
                    },
                    items: _filteredItems.map((option) {
                      return DropdownMenuItem<String>(
                        value: option['id'].toString(),
                        child: Text(option['name']),
                      );
                    }).toList(),
                    decoration: InputDecoration(
                      labelText: 'Бараа сонгох',
                    ),
                  )),
              if (_allDelivery.length > 0)
                Expanded(
                  child: Container(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        children: [
                          SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: DataTable(
                                columns: const [
                                  DataColumn(label: Text('Өдөр')),
                                  DataColumn(label: Text('Хүргэсэн тоо')),
                                  DataColumn(label: Text('Дэлгэрэнгүй')),
                                ],
                                rows: _allDelivery.map<DataRow>((dtl) {
                                  return DataRow(cells: [
                                    DataCell(Text(dtl['date'].toString())),
                                    DataCell(Text(
                                        dtl['delivered_count'].toString() ??
                                            0.toString())),
                                    DataCell(
                                      ElevatedButton(
                                        onPressed: () {
                                          fetch_delivery_detail(
                                              dtl['date'].toString());
                                        },
                                        child: Text('>'),
                                      ),
                                    ),
                                  ]);
                                }).toList(),
                              ))
                        ],
                      ),
                    ),
                  ),
                )
            ],
          )),
          Center(
              child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: TextField(
                  onChanged: (value) {
                    filterItem(value);
                  },
                  decoration: InputDecoration(
                    labelText: 'Хайх',
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: DropdownButtonFormField<String>(
                    value: selected_item,
                    onChanged: (value) {
                      setState(() {
                        selected_item = value;
                      });
                      fetch_other_item_remainder();
                    },
                    items: _filteredItems.map((option) {
                      return DropdownMenuItem<String>(
                        value: option['id'].toString(),
                        child: Text(option['name']),
                      );
                    }).toList(),
                    decoration: InputDecoration(
                      labelText: 'Бараа сонгох',
                    ),
                  )),
              if (_allRemainders.length > 0)
                Expanded(
                  child: Container(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        children: [
                          SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: DataTable(
                                sortColumnIndex: _sortColumnIndex1,
                                sortAscending: _sortAscending1,
                                columns: [
                                  DataColumn(
                                    label: Text('Жолооч(Агуулахын нэр)'),
                                    onSort: (columnIndex, ascending) {
                                      setState(() {
                                        _sortColumnIndex1 = columnIndex;
                                        _sortAscending1 = ascending;
                                        _sort1<String>(
                                            (dtl) => dtl['warehouse']['name']);
                                      });
                                    },
                                  ),
                                  DataColumn(
                                    label: Text('Цэвэр үлдэгдэл'),
                                    onSort: (columnIndex, ascending) {
                                      setState(() {
                                        _sortColumnIndex1 = columnIndex;
                                        _sortAscending1 = ascending;
                                        _sort1<num>((dtl) =>
                                            int.tryParse(
                                                dtl['pure_remainder']) ??
                                            0);
                                      });
                                    },
                                  ),
                                ],
                                rows: _allRemainders.map<DataRow>((dtl) {
                                  return DataRow(cells: [
                                    DataCell(Text(dtl['warehouse']['name'])),
                                    DataCell(Text(
                                        dtl['pure_remainder'].toString() ??
                                            0.toString())),
                                  ]);
                                }).toList(),
                              ))
                        ],
                      ),
                    ),
                  ),
                )
            ],
          )),
        ],
      ),
    );
  }

  void createNewTransaction(BuildContext context) {}
}
