import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import '../tools/tool_importer.dart';

getCard(index, order, array_size) {
  int total_price = get_total_price(order['items'], order['status']);
  int total_quantity = get_total_quantity(order['items'], order['status']);
  OrderStatusStyleObj status_obj = get_order_status_style(order['status'],
      order['order_type'], order['old_driver_id'], order['driver_id']);

  // Display your list item widget here
  return Card(
    elevation: 1.5,
    child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Text(order['customer']['phone_number']),
                    InkWell(
                      child: Icon(Icons.call),
                      onTap: () async {
                        final Uri launchUri = Uri(
                            scheme: 'tel',
                            path: '${order['customer']['phone_number']}');
                        await launchUrl(launchUri);
                      },
                    ),
                    InkWell(
                      child: Icon(Icons.sms),
                      onTap: () async {
                        final Uri launchUri = Uri(
                            scheme: 'sms',
                            path: '${order['customer']['phone_number']}');
                        await launchUrl(launchUri);
                      },
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                      decoration: BoxDecoration(
                        color: status_obj.color,
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: Text(
                        status_obj.text,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    Text(
                      "\nХүргэлтийн өдөр: " +
                          order['delivery_date'].toString().substring(0, 10),
                      style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Wrap(
                    // padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    children: [
                      Text(
                        order['customer']['address'],
                        style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
                      )
                    ],
                  ),
                  Wrap(
                    // padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    children: [],
                  ),
                  if (order['items'].length > 0)
                    Container(
                      height:
                          250, // Specify a fixed or desired height for the container
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          children: [
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: DataTable(
                                columns: const [
                                  DataColumn(label: Text('Барааны нэр')),
                                  DataColumn(label: Text('Захиалсан')),
                                  DataColumn(label: Text('Хүргэсэн')),
                                  DataColumn(label: Text('Нэгжийэ үнэ')),
                                ],
                                rows: order['items'].map<DataRow>((item) {
                                  return DataRow(cells: [
                                    DataCell(Text(item['item']['name'])),
                                    DataCell(
                                        Text('${item['count'].toString()}ш')),
                                    DataCell(Text(
                                        '${item['delivered_count'].toString()}ш')),
                                    DataCell(Text(NumberFormat('#,###').format(
                                            int.parse(
                                                item['price'].toString())) +
                                        '₮'))
                                  ]);
                                }).toList(),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                        "Захиалга авсан өдөр: " +
                                            order['created_at']
                                                .toString()
                                                .substring(0, 10),
                                        style: TextStyle(
                                            color:
                                                Color.fromARGB(255, 0, 0, 0)),
                                      ),
                                      Text(
                                          'Нийт тоо ширхэг: $total_quantity ш'),
                                      Text(
                                        'Нийт үнэ: ${NumberFormat('#,###').format(int.parse(total_price.toString()))}₮',
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                ],
              ),
            ],
          ),
        )),
  );
}
