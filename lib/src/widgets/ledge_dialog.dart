import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../tools/tool_importer.dart';

class LedgeDialog extends StatefulWidget {
  final VoidCallback callback;

  LedgeDialog({required this.callback});

  @override
  _LedgeDialogState createState() => _LedgeDialogState();
}

class _LedgeDialogState extends State<LedgeDialog> {
  late DateTime _startDate = DateTime.now().subtract(Duration(days: 15));
  late DateTime _endDate = DateTime.now();
  List _allData = [];
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {
    try {
      String url = Global.ledgeUrl;
      Map<String, String> headers = {
        'Authorization': 'Bearer ${Global.loginToken}'
      };
      Map<String, String> queryParams = {
        'start': '${_startDate.year}-${_startDate.month}-${_startDate.day}',
        'end': '${_endDate.year}-${_endDate.month}-${_endDate.day}',
        'driver_id': Global.userId.toString(),
      };

      String queryString = Uri(queryParameters: queryParams).query;
      var response =
          await http.get(Uri.parse('$url?$queryString'), headers: headers);
      DateFormat inputFormat = DateFormat("E, d MMM yyyy HH:mm:ss 'GMT'");
      DateFormat outputFormat = DateFormat("yyyy-MM-dd");

      if (response.statusCode == 200) {
        var tmp = json.decode(response.body)['workdays'];
        tmp.forEach((ledge) {
          DateTime dateTime = inputFormat.parse(ledge["end_time"]);
          ledge["end_time"] = outputFormat.format(dateTime);
        });
        setState(() {
          _allData = tmp;
        });
      }
    } on Object catch (error, stackTrace) {
      print('Request error: $error');
      print('Stack trace: $stackTrace');
    }
  }

  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _startDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _startDate) {
      setState(() {
        _startDate = picked;
      });
    }
  }

  Future<void> _selectEndDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: _endDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _endDate) {
      setState(() {
        _endDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Тайлан'),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            Row(
              children: [
                ElevatedButton(
                  onPressed: () => _selectStartDate(context),
                  child: Text(
                      'Эхлэх огноо: ${_startDate.toString().split(' ')[0]}'),
                ),
                SizedBox(width: 8),
                ElevatedButton(
                  onPressed: () => _selectEndDate(context),
                  child: Text(
                      'Дуусах огноо: ${_endDate.toString().split(' ')[0]}'),
                ),
                ElevatedButton(
                  onPressed: () {
                    // Handle submit button press
                    // Fetch data from API based on selected date range and filter
                    fetchData();
                  },
                  child: Text('Мэдээлэл татах'),
                ),
              ],
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: DataTable(
                    columns: const [
                      DataColumn(label: Text('Өдөр')),
                      DataColumn(label: Text('Нийт хүргэлт')),
                      DataColumn(label: Text('Хүргэсэн тоо')),
                      DataColumn(label: Text('Нийт хүргэлтийн мөнгө')),
                      DataColumn(label: Text('Нэгж хүргэлтийн мөнгө')),
                      DataColumn(label: Text('Ниийт орлого')),
                      DataColumn(label: Text('Ажлаа цагтаа эхлүүлсэн эсэх')),
                      DataColumn(
                          label: Text('Мөнгөө хугацаандаа шилжүүлсэн эсэх')),
                    ],
                    rows: _allData.map<DataRow>((item) {
                      return DataRow(cells: [
                        DataCell(Text(item['end_time'] ?? 'N/A')),
                        DataCell(
                            Text(item['ordered_count']?.toString() ?? '0')),
                        DataCell(Text(item['order_count']?.toString() ?? '0')),
                        DataCell(Text(NumberFormat('#,###').format(int.parse(
                                item['salary_amount']?.toString() ?? '0')) +
                            '₮')),
                        DataCell(Text(
                            item['per_delivery_price']?.toString() ?? '0')),
                        DataCell(Text(NumberFormat('#,###').format(int.parse(
                                item['invoice_amount']?.toString() ?? '0')) +
                            '₮')),
                        DataCell(Text(item['late_invoice']?.toString() ?? '0')),
                        DataCell(Text(item['late_work']?.toString() ?? '0')),
                      ]);
                    }).toList(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
