import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../tools/globals.dart';
import '../tools/toast.dart';

class ItemSelectorWidget extends StatefulWidget {
  final Function callbackFunction;
  const ItemSelectorWidget({required this.callbackFunction});

  @override
  _ItemSelectorWidgetState createState() => _ItemSelectorWidgetState();
}

class _ItemSelectorWidgetState extends State<ItemSelectorWidget> {
  TextEditingController _filterController = TextEditingController();
  List _filteredData = [];
  List _allData = [];

  @override
  void initState() {
    super.initState();
    fetchData();
    final selectedItems = Global.selectedOrder['items'];

    for (final item in Global.items) {
      if (selectedItems
          .any((selectedItem) => selectedItem['item_id'] == item['id'])) {
        continue;
      }
      _allData.add({
        'item_id': item['id'],
        'name': item['name'],
        'count': 1,
        'delivered_count': 1,
        'price': item['price'],
        'item': {'name': item['name'], 'price': item['price']},
      });
    }
  }

  @override
  void dispose() {
    _filterController.dispose();
    super.dispose();
  }

  Future<void> fetchData() async {
    setState(() {
      _filteredData = _allData;
    });
  }

  void filterData(String filter) {
    setState(() {
      _filteredData = _allData.where((data) {
        final item_name = data['name']!.toLowerCase();
        final searchTerm = filter.toLowerCase();
        return item_name.contains(searchTerm);
      }).toList();
    });
  }

  void addItemToOrder(item) {
    setState(() {
      Global.selectedOrder['items'].add(item);
      _allData.remove(item);
      _filteredData = _allData.where((data) {
        final item_name = data['name']!.toLowerCase();
        final searchTerm = _filterController.text.toLowerCase();
        return item_name.contains(searchTerm);
      }).toList();
      widget.callbackFunction();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            controller: _filterController,
                            onChanged: filterData,
                            decoration: InputDecoration(
                              labelText: 'Хайх',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Expanded(
            child: Visibility(
              visible: _filteredData.isNotEmpty,
              replacement:
                  Text('Бүх бараа сонгогдсон байна . . . '), // Empty message
              child: ListView.builder(
                itemCount: _filteredData.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                      onDoubleTap: () {
                        addItemToOrder(_filteredData[index]);
                      },
                      child: ListTile(
                        title: Text(_filteredData[index]['name']),
                        trailing: Text(NumberFormat('#,###').format(int.parse(
                                _filteredData[index]['price'].toString())) +
                            '₮'),
                      ));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

fetchItems() async {
  try {
    Map<String, String> headers = {
      'Authorization': 'Bearer ${Global.loginToken}'
    };
    var response = await http.get(Uri.parse('${Global.itemsUrl}?limit=1000'),
        headers: headers);
    if (response.statusCode == 200) {
      var items = json.decode(response.body)['items'];
      Global.items = items;
    }
  } on Object catch (error, stackTrace) {
    // Handle request error
    print('Request error: $error');
    print('Stack trace: $stackTrace');
  }
}

void addItemToOrder(context, updateValue) {
  Global.itemsToSave = [];
  for (final selectedItem in Global.selectedOrder['items']) {
    Global.itemsToSave.add({
      "item_id": selectedItem["item_id"],
      "count": selectedItem["count"],
      "name": selectedItem["item"]["name"],
      "price": selectedItem["price"],
    });
  }
  Navigator.of(context).pop();
  updateValue();
}

Future<void> showItemSelectDialog(BuildContext context, updateValue) async {
  await fetchItems();

  showDialog<void>(
    context: context,
    builder: (context) => Dialog.fullscreen(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Scaffold(
            appBar: AppBar(
              title: const Text('Бараа сонгох'),
              centerTitle: false,
              leading: IconButton(
                icon: const Icon(Icons.close),
                onPressed: () => Navigator.of(context).pop(),
              ),
              actions: [
                TextButton(
                  child: const Text('Хаах'),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
            body: Column(children: [
              Container(
                  height: MediaQuery.of(context).size.height - 170,
                  child: ItemSelectorWidget(callbackFunction: updateValue)),
              if (Global.selectedOrder["status"] != "NEW")
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: double.infinity,
                      height: 50,
                      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      child: ElevatedButton(
                        onPressed: () => Navigator.of(context).pop(),
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.green),
                        ),
                        child: Text('Болсон'),
                      ),
                    ),
                  ],
                ),
            ])),
      ),
    ),
  );
}
