import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../routing.dart';
import '../tools/tool_importer.dart';

class NotifLogDialog extends StatefulWidget {
  final VoidCallback callback;

  NotifLogDialog({required this.callback});

  @override
  _NotifLogDialogState createState() => _NotifLogDialogState();
}

class _NotifLogDialogState extends State<NotifLogDialog> {
  List _allData = [];
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {
    var tmp = [];
    try {
      for (int i = 0; i < Global.notifications.length; i++) {
        var obj = {};

        obj["index"] = i;
        obj["status"] = 'Бараа зөвшөөрөх';
        if (Global.notifications[i]['status'].toString() == 'WAITING')
          obj["status"] = 'Хүргэх';
        if (Global.notifications[i]['status'].toString() == 'DRIVERREQUEST')
          obj["status"] = 'Хүргэлт хүлээж авах';
        obj["phone_number"] = Global.notifications[i]["phone_number"];
        obj["address"] = Global.notifications[i]["address"];
        obj['type'] = 'Хүргэлт';
        if (Global.notifications[i]['status'].toString() == 'DRIVERREQUEST')
          obj['type'] = 'Хүргэлт шилжүүлэг';
        if (Global.notifications[i]['type'].toString() == 'transaction')
          obj["type"] = 'Бараа шилжүүлэг';

        tmp.add(obj);
      }
      setState(() {
        _allData = tmp;
      });
    } on Object catch (error, stackTrace) {
      print('Request error: $error');
      print('Stack trace: $stackTrace');
    }
  }

  RouteState get _routeState => RouteStateScope.of(context);

  void processTransaction(transaction) async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Confirm'),
          content: Text('Шилжүүлгийг хүлээн авах уу?'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: Text('Үгүй'),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: Text('Тийм'),
            ),
          ],
        );
      },
    ).then((value) async {
      String respo = "APPROVE";
      if (!value) respo = "DECLINE";
      String url =
          '${Global.transactionUrl}/${transaction['id'].toString()}/status';
      Map<String, String> headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${Global.loginToken}'
      };
      var response = await http.put(
        Uri.parse(url),
        headers: headers,
        body: jsonEncode({"response": respo}),
      );
      print(response.body);
    });
  }

  redirect(index, type) {
    if (type != 'transaction') {
      Global.selectedOrder = Global.notifications[index];
      _routeState.go('/order/${Global.notifications[index]["id"]}');
    } else {
      processTransaction(Global.tmp_orders[index]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("  "),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: DataTable(
                    columns: const [
                      DataColumn(label: Text('Төлөв')),
                      DataColumn(label: Text('Утас')),
                      DataColumn(label: Text('Хаяг /хаанаас/')),
                      DataColumn(label: Text('Төрөл')),
                      DataColumn(label: Text('   ')),
                    ],
                    rows: _allData.map<DataRow>((item) {
                      return DataRow(cells: [
                        DataCell(Text(item['status'].toString())),
                        DataCell(Text(item['phone_number']?.toString() ?? '0')),
                        DataCell(Text(item['address']?.toString() ?? '0')),
                        DataCell(Text(item['type'].toString())),
                        DataCell(ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.blueGrey),
                          ),
                          onPressed: () async {
                            // call your async function here
                            redirect(item['index'], item['type']);
                          },
                          child: Text('Хүргэх /Зөвшөөрөх/'),
                        )),
                      ]);
                    }).toList(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
