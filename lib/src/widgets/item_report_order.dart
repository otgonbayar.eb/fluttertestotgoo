import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../tools/tool_importer.dart';

class ItemReportDialog extends StatefulWidget {
  final VoidCallback callback;

  ItemReportDialog({required this.callback});

  @override
  _ItemReportDialogState createState() => _ItemReportDialogState();
}

class _ItemReportDialogState extends State<ItemReportDialog> {
  late DateTime _startDate = DateTime.now().subtract(Duration(days: 15));
  late DateTime _endDate = DateTime.now();
  List _allData = [];
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {
    var tmp = [];
    try {
      for (int i = 0; i < Global.tmp_item_orders.length; i++) {
        var obj = {};
        obj["phone_number"] = Global.tmp_item_orders[i]["phone_number"];
        obj["address"] = Global.tmp_item_orders[i]["address"];
        obj["delivered_date"] = Global.tmp_item_orders[i]["delivered_date"];
        obj["delivered_count"] = Global.tmp_item_orders[i]["delivered_count"];
        obj["price"] = Global.tmp_item_orders[i]["price"];
        tmp.add(obj);
      }
      setState(() {
        _allData = tmp;
      });
    } on Object catch (error, stackTrace) {
      print('Request error: $error');
      print('Stack trace: $stackTrace');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Тайлан'),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: DataTable(
                    columns: const [
                      DataColumn(label: Text('Хүргэсэн хугацаа')),
                      DataColumn(label: Text('Утасны дугаар')),
                      DataColumn(label: Text('Хаяг')),
                      DataColumn(label: Text('Хүргэсэн тоо')),
                      DataColumn(label: Text('Үнэ')),
                    ],
                    rows: _allData.map<DataRow>((item) {
                      return DataRow(cells: [
                        DataCell(Text(item['delivered_date'] ?? '-')),
                        DataCell(Text(item['phone_number']?.toString() ?? '-')),
                        DataCell(Text(item['address']?.toString() ?? '0')),
                        DataCell(
                            Text(item['delivered_count']?.toString() ?? '0')),
                        DataCell(Text(NumberFormat('#,###').format(
                                int.parse(item['price']?.toString() ?? '0')) +
                            '₮'))
                      ]);
                    }).toList(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
