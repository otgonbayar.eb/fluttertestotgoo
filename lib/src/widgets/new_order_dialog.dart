import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../tools/globals.dart';
import '../tools/toast.dart';

class ItemSelectorWidget extends StatefulWidget {
  final Function callbackFunction;
  const ItemSelectorWidget({required this.callbackFunction});

  @override
  _ItemSelectorWidgetState createState() => _ItemSelectorWidgetState();
}

class _ItemSelectorWidgetState extends State<ItemSelectorWidget> {
  TextEditingController _filterController = TextEditingController();
  final _customerPhoneNumberController = TextEditingController();
  final _customerAddressController = TextEditingController();
  List _filteredData = [];
  List _allData = [];

  @override
  void initState() {
    super.initState();
    fetchData();
    fetchItems();
    for (final item in Global.items) {
      _allData.add({
        'item_id': item['id'],
        'name': item['name'],
        'count': 0,
        'price': item['price'],
        'item': {'name': item['name'], 'price': item['price']},
      });
    }
  }

  @override
  void dispose() {
    _filterController.dispose();
    super.dispose();
  }

  Future<void> fetchData() async {
    setState(() {
      _filteredData = _allData;
    });
  }

  void saveOrder(context) async {
    Global.itemsToSave = [];
    for (final selectedItem
        in _filteredData.where((item) => item['count'] > 0).toList()) {
      Global.itemsToSave.add({
        "item_id": selectedItem["item_id"],
        "count": selectedItem["count"],
        "name": selectedItem["item"]["name"],
        "price": selectedItem["price"],
      });
    }
    String url = '${Global.ordersUrl}/create_by_driver';
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${Global.loginToken}'
    };
    try {
      await http.post(
        Uri.parse(url),
        headers: headers,
        body: jsonEncode({
          "customer_phone_number":
              _customerPhoneNumberController.text.toString(),
          "customer_address": _customerAddressController.text.toString(),
          "operator_id": Global.userId.toString(),
          "driver_id": Global.userId.toString(),
          "items": Global.itemsToSave
        }),
      );
      widget.callbackFunction();
      Navigator.of(context).pop();
      widget.callbackFunction();
    } on Object catch (error, stackTrace) {
      MinimalToaster.showToast("Алдаа гарлаа");
      print('Request error: $error');
      print('Stack trace: $stackTrace');
    }
  }

  void filterData(String filter) {
    setState(() {
      _filteredData = _allData.where((data) {
        final item_name = data['name']!.toLowerCase();
        final searchTerm = filter.toLowerCase();
        return item_name.contains(searchTerm);
      }).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            controller: _filterController,
                            onChanged: filterData,
                            decoration: InputDecoration(
                              labelText: 'Хайх',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
              height: 350,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: DataTable(
                  columns: const [
                    DataColumn(label: Text('Барааны нэр')),
                    DataColumn(label: Text('Тоо ширхэг')),
                    DataColumn(label: Text('Нэгжийэ үнэ')),
                  ],
                  rows: _filteredData.map<DataRow>((item) {
                    return DataRow(cells: [
                      DataCell(Text(item['item']['name'])),
                      // DataCell(Text('${item['count'].toString()}ш')),
                      DataCell(
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IconButton(
                              icon: const Icon(Icons.remove),
                              onPressed: () {
                                setState(() {
                                  item['count']--;
                                });
                              },
                            ),
                            Text(item['count'].toString()),
                            IconButton(
                              icon: const Icon(Icons.add),
                              onPressed: () {
                                setState(() {
                                  item['count']++;
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                      DataCell(Text(NumberFormat('#,###')
                              .format(int.parse(item['price'].toString())) +
                          '₮'))
                    ]);
                  }).toList(),
                ),
              )),
          if (Global.selectedOrder["status"] == "NEW")
            TextFormField(
              maxLength: 8,
              inputFormatters: [
                FilteringTextInputFormatter.digitsOnly,
              ],
              maxLines: null,
              decoration: InputDecoration(
                labelText: 'Утасны дугаар',
                hintText: 'Утасны дугаар',
                border: OutlineInputBorder(),
              ),
              controller: _customerPhoneNumberController,
            ),
          if (Global.selectedOrder["status"] == "NEW")
            TextFormField(
              maxLines: null,
              decoration: InputDecoration(
                labelText: 'Гэрийн хаяг',
                hintText: 'Хаяг',
                border: OutlineInputBorder(),
              ),
              controller: _customerAddressController,
            ),
          if (Global.selectedOrder["status"] == "NEW")
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: ElevatedButton(
                    onPressed: () => {saveOrder(context)},
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.green),
                    ),
                    child: Text('Хадгалах'),
                  ),
                ),
              ],
            )
        ],
      ),
    );
  }
}

fetchItems() async {
  try {
    Map<String, String> headers = {
      'Authorization': 'Bearer ${Global.loginToken}'
    };
    var response = await http.get(Uri.parse('${Global.itemsUrl}?limit=1000'),
        headers: headers);
    if (response.statusCode == 200) {
      var items = json.decode(response.body)['items'];
      Global.items = items;
    }
  } on Object catch (error, stackTrace) {
    // Handle request error
    print('Request error: $error');
    print('Stack trace: $stackTrace');
  }
}

void addItemToOrder(context, updateValue) {
  Global.itemsToSave = [];
  for (final selectedItem in Global.selectedOrder['items']) {
    Global.itemsToSave.add({
      "item_id": selectedItem["item_id"],
      "count": selectedItem["count"],
      "name": selectedItem["item"]["name"],
      "price": selectedItem["price"],
    });
  }
  Navigator.of(context).pop();
  updateValue();
}

Future<void> showNewOrderDialog(BuildContext context, updateValue) async {
  await fetchItems();

  showDialog<void>(
    context: context,
    builder: (context) => Dialog.fullscreen(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Scaffold(
            appBar: AppBar(
              title: const Text('Бараа сонгох'),
              centerTitle: false,
              leading: IconButton(
                icon: const Icon(Icons.close),
                onPressed: () => Navigator.of(context).pop(),
              ),
              actions: [
                TextButton(
                  child: const Text('Хаах'),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
            body: Column(children: [
              Container(
                  height: MediaQuery.of(context).size.height - 250,
                  child: ItemSelectorWidget(callbackFunction: updateValue)),
            ])),
      ),
    ),
  );
}
