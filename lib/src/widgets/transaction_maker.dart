import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../tools/tool_importer.dart';

class TransactionDialog extends StatefulWidget {
  final VoidCallback callback;

  TransactionDialog({required this.callback});

  @override
  _TransactionDialogState createState() => _TransactionDialogState();
}

class _TransactionDialogState extends State<TransactionDialog> {
  String? selected_warehouse;
  String? selected_type = "0";
  List transaction_type_options = [
    {'type': '0', 'label': 'Бараа шилжүүлэх'},
    // {'type': '1', 'label': 'Үйлчлүүлэгч бараа буцаах'},
  ];
  List warehouses = [];
  List filtered_warehouses = [];
  TextEditingController _filterController = TextEditingController();
  final _customerPhoneNumberController = TextEditingController();
  List _filteredData = [];
  List _allData = [];
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  void filterData(String filter) {
    setState(() {
      _filteredData = _allData.where((data) {
        final item_name = data['name']!.toLowerCase();
        final searchTerm = filter.toLowerCase();
        return item_name.contains(searchTerm);
      }).toList();
    });
  }

  Future<void> fetchData() async {
    try {
      String url = Global.usersUrl;
      Map<String, String> headers = {
        'Authorization': 'Bearer ${Global.loginToken}'
      };
      var response_warehouse =
          await http.get(Uri.parse('$url'), headers: headers);
      var response_item = await http
          .get(Uri.parse('${Global.itemsUrl}?limit=1000'), headers: headers);
      if (response_item.statusCode == 200) {
        var items = json.decode(response_item.body)['items'];
        Global.items = items;
      }

      if (response_warehouse.statusCode == 200) {
        var users = json.decode(response_warehouse.body)['users'];

        _allData = [];
        for (final item in Global.items) {
          _allData.add({
            'item_id': item['id'],
            'name': item['name'],
            'count': 0,
            'price': item['price'],
            'item': {'name': item['name'], 'price': item['price']},
          });
        }

        setState(() {
          warehouses =
              users.where((house) => house['role'] != "OPERATOR").toList();
          ;
          filtered_warehouses = warehouses;
          _filteredData = _allData;
        });
      }
    } on Object catch (error, stackTrace) {
      print('Request error: $error');
      print('Stack trace: $stackTrace');
    }
  }

  Future<void> saveTransaction() async {
    String from;
    String to;
    Global.itemsToSave = [];
    for (final selectedItem
        in _filteredData.where((item) => item['count'] > 0).toList()) {
      Global.itemsToSave.add({
        "item_id": selectedItem["item_id"],
        "count": selectedItem["count"],
        "name": selectedItem["item"]["name"],
        "price": selectedItem["price"],
      });
    }
    if (Global.itemsToSave.length > 0) {
      Map<String, String> headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${Global.loginToken}'
      };

      if (selected_type == "0") {
        from = Global.userId.toString();
        to = selected_warehouse.toString();
        var resp = await http.post(
          Uri.parse(Global.transactionUrl),
          headers: headers,
          body: jsonEncode({
            "customer_phone_number":
                _customerPhoneNumberController.text.toString(),
            "from_id": int.parse(from),
            "to_id": int.parse(to),
            "items": Global.itemsToSave
          }),
        );
      } else {
        to = Global.userId.toString();
        print(jsonEncode({
          "customer_phone_number":
              _customerPhoneNumberController.text.toString(),
          "to_id": int.parse(to),
          "items": Global.itemsToSave
        }));
        var resp = await http.post(
          Uri.parse(Global.transactionUrl),
          headers: headers,
          body: jsonEncode({
            "customer_phone_number":
                _customerPhoneNumberController.text.toString(),
            "to_id": int.parse(to),
            "items": Global.itemsToSave
          }),
        );
      }
      widget.callback();
      Navigator.of(context).pop();
    } else
      MinimalToaster.showToast("Бараа заавал сонгон уу");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Бараа шилжүүлэх'),
      ),
      body: Container(
        child: Column(
          children: [
            DropdownButton<String>(
              value: selected_type,
              hint: Text('Төрөл сонгох'),
              onChanged: (value) {
                setState(() {
                  selected_type = value;
                });
              },
              items: transaction_type_options.map((option) {
                return DropdownMenuItem<String>(
                  value: option['type'],
                  child: Text(option['label']),
                );
              }).toList(),
            ),
            if (selected_type == "1")
              TextFormField(
                maxLength: 8,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                ],
                maxLines: null,
                decoration: InputDecoration(
                  labelText: 'Утасны дугаар',
                  hintText: 'Утасны дугаар',
                  border: OutlineInputBorder(),
                ),
                controller: _customerPhoneNumberController,
              ),
            if (selected_type == "0")
              DropdownButtonFormField<String>(
                value: selected_warehouse,
                onChanged: (value) {
                  setState(() {
                    selected_warehouse = value;
                  });
                },
                items: filtered_warehouses.map((option) {
                  return DropdownMenuItem<String>(
                    value: option['id'].toString(),
                    child: Text(option['name']),
                  );
                }).toList(),
                decoration: InputDecoration(
                  labelText: 'шилжүүлэх жолооч',
                ),
              ),
            Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: TextField(
                                  controller: _filterController,
                                  onChanged: filterData,
                                  decoration: InputDecoration(
                                    labelText: 'Барааны нэрээр хайх',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                    height: 300,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: DataTable(
                        columns: const [
                          DataColumn(label: Text('Барааны нэр')),
                          DataColumn(label: Text('Тоо ширхэг')),
                          DataColumn(label: Text('Нэгжийэ үнэ')),
                        ],
                        rows: _filteredData.map<DataRow>((item) {
                          return DataRow(cells: [
                            DataCell(Text(item['item']['name'])),
                            DataCell(
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  IconButton(
                                    icon: const Icon(Icons.remove),
                                    onPressed: () {
                                      setState(() {
                                        item['count']--;
                                      });
                                    },
                                  ),
                                  Text(item['count'].toString()),
                                  IconButton(
                                    icon: const Icon(Icons.add),
                                    onPressed: () {
                                      setState(() {
                                        item['count']++;
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ),
                            DataCell(Text(NumberFormat('#,###').format(
                                    int.parse(item['price'].toString())) +
                                '₮'))
                          ]);
                        }).toList(),
                      ),
                    )),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: double.infinity,
                      height: 50,
                      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      child: ElevatedButton(
                        onPressed: () => {saveTransaction()},
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.green),
                        ),
                        child: Text('Хадгалах'),
                      ),
                    ),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
