import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../routing.dart';
import '../tools/tool_importer.dart';

class MoneyLogDialog extends StatefulWidget {
  final VoidCallback callback;

  MoneyLogDialog({required this.callback});

  @override
  _MoneyLogDialogState createState() => _MoneyLogDialogState();
}

class _MoneyLogDialogState extends State<MoneyLogDialog> {
  List _allData = [];
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {
    var tmp = [];
    try {
      for (int i = 0; i < Global.tmp_orders.length; i++) {
        var obj = {};
        obj["index"] = i;
        obj["delivered_date"] = Global.tmp_orders[i]["delivered_date"];
        obj["phone_number"] = Global.tmp_orders[i]["customer"]["phone_number"];
        obj["address"] = Global.tmp_orders[i]["customer"]["address"];
        obj["money"] = 0;
        if (Global.tmp_orders[i]['driver_note'] != null &&
            Global.tmp_orders[i]['driver_note'] != "") {
          Map<String, dynamic> driverNote =
              jsonDecode(Global.tmp_orders[i]['driver_note']);
          if (driverNote['cash'] != "" && Global.selcetedMoneyLog == "Бэлэн") {
            obj["money"] = int.parse(driverNote['cash']);
          }
          if (driverNote['remit'] != "" && Global.selcetedMoneyLog == "Данс") {
            obj["money"] = int.parse(driverNote['remit']);
          }
          if (driverNote['owe'] != "" && Global.selcetedMoneyLog == "Авлага") {
            obj["money"] = int.parse(driverNote['owe']);
          }
        }
        if (obj["money"] > 0) tmp.add(obj);
      }
      setState(() {
        _allData = tmp;
      });
    } on Object catch (error, stackTrace) {
      print('Request error: $error');
      print('Stack trace: $stackTrace');
    }
  }

  RouteState get _routeState => RouteStateScope.of(context);

  redirect(index) {
    Global.selectedOrder = Global.tmp_orders[index];
    _routeState.go('/order/${Global.tmp_orders[index]["id"]}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Тайлан'),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            Text(Global.selcetedMoneyLog),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: DataTable(
                    columns: const [
                      DataColumn(label: Text('Хүргэсэн хугацаа')),
                      DataColumn(label: Text('Утасны дугаар')),
                      DataColumn(label: Text('Хаяг')),
                      DataColumn(label: Text('Мөнгөн дүн')),
                      DataColumn(label: Text('   ')),
                    ],
                    rows: _allData.map<DataRow>((item) {
                      return DataRow(cells: [
                        DataCell(Text(item['delivered_date'] ?? '-')),
                        // DataCell(GestureDetector(
                        //     onDoubleTap: redirect(item["index"]),
                        //     child:
                        //         Text(item['phone_number']?.toString() ?? '-'))),

                        DataCell(Text(item['phone_number']?.toString() ?? '0')),
                        DataCell(Text(item['address']?.toString() ?? '0')),
                        DataCell(Text(NumberFormat('#,###').format(
                                int.parse(item['money']?.toString() ?? '0')) +
                            '₮')),
                        DataCell(ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.blueGrey),
                          ),
                          onPressed: () async {
                            // call your async function here
                            redirect(item['index']);
                          },
                          child: Text('Засварлах'),
                        )),
                      ]);
                    }).toList(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
