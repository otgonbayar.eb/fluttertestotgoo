import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../tools/tool_importer.dart';
import 'package:flutter/material.dart';

class ChangePasswordDialog extends StatefulWidget {
  @override
  _ChangePasswordDialogState createState() => _ChangePasswordDialogState();
}

class _ChangePasswordDialogState extends State<ChangePasswordDialog> {
  TextEditingController _passwordController1 = TextEditingController();
  TextEditingController _passwordController2 = TextEditingController();
  bool _passwordsMatch = false;

  @override
  void dispose() {
    _passwordController1.dispose();
    _passwordController2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Нүүц үг солих'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            controller: _passwordController1,
            obscureText: true,
            decoration: InputDecoration(
              labelText: 'Шинэ нууц үг',
            ),
            onChanged: (_) => _checkPasswordsMatch(),
          ),
          TextField(
            controller: _passwordController2,
            obscureText: true,
            decoration: InputDecoration(
              labelText: 'Нууц үг давтах',
            ),
            onChanged: (_) => _checkPasswordsMatch(),
          ),
          if (_passwordsMatch)
            ElevatedButton(
              onPressed: () async {
                print(Global.loginToken);
                try {
                  Map<String, String> headers = {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ${Global.loginToken}'
                  };
                  final response = await http.put(
                    Uri.parse('${Global.usersUrl}/${Global.userId}'),
                    headers: headers,
                    body: jsonEncode({'password': _passwordController1.text}),
                  );
                  if (response.statusCode == 201) {
                    MinimalToaster.showToast("Нууц үг амжилттай солигдлоо");
                  }
                } on Object catch (error, stackTrace) {
                  MinimalToaster.showToast("Алдаа гарлаа");
                  print('Request error: $error');
                  print('Stack trace: $stackTrace');
                }
                Navigator.of(context).pop();
              },
              child: Text('Нууц үг солих'),
            ),
        ],
      ),
    );
  }

  void _checkPasswordsMatch() {
    setState(() {
      _passwordsMatch = _passwordController1.text == _passwordController2.text;
    });
  }
}
