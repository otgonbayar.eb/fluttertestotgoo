class Item {
  int id;
  String name;
  int price;
  int count;

  Item(this.id, this.name, this.price, this.count);

  int getId() {
    return id;
  }

  void setId(int value) {
    id = value;
  }

  String getName() {
    return this.name;
  }

  void setName(String value) {
    this.name = value;
  }

  int getPrice() {
    return this.price;
  }

  void setSrice(int value) {
    price = value;
  }

  int getCount() {
    return this.count;
  }

  setCount(int value) {
    this.count = value;
  }

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      json['id'] as int,
      json['name'] as String,
      json['price'] as int,
      json['count'] as int,
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'count': count,
    };
  }
}
