import 'item.dart';

class Order {
  int id;
  int driver_id;
  int operator_id;
  String phone_number;
  String address;
  String status;
  List<Item> items;

  Order(this.id, this.driver_id, this.operator_id, this.phone_number,
      this.address, this.status, this.items);

  // Getter and setter for 'id'
  int getId() {
    return this.id;
  }

  void setId(int id) {
    this.id;
  }

  // Getter and setter for 'phone_number'
  String getPhone_number() {
    return this.phone_number;
  }

  setPhone_number(String phone_number) {
    this.phone_number = phone_number;
  }

  String getAddress() {
    return this.address;
  }

  void setAddress(String address) {
    this.address = address;
  }

  // Getter and setter for 'status'
  String getStatus() {
    return this.status;
  }

  void setStatus(String status) {
    this.status = status;
  }

  // Getter and setter for 'items'
  List<Item> getItems() {
    return this.items;
  }

  void setItems(List<Item> items) {
    this.items = items;
  }

  // Factory function to deserialize JSON data
  factory Order.fromJson(Map<String, dynamic> json) {
    final items = (json['items'] as List)
        .map((itemJson) => Item.fromJson(itemJson))
        .toList();
    return Order(
      json['id'] as int,
      json['driver_id'] as int,
      json['operator_id'] as int,
      json['phone_number'] as String,
      json['address'] as String,
      json['status'] as String,
      items,
    );
  }

  // Method to serialize object data to JSON
  Map<String, dynamic> toJson() => {
        'id': id,
        'driver_id': driver_id,
        'operator_id': operator_id,
        'phone_number': phone_number,
        'address': address,
        'status': status,
        'items': items.map((item) => item.toJson()).toList(),
      };
}
