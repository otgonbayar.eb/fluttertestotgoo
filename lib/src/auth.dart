import 'package:flutter/widgets.dart';
import 'tools/toast.dart';
// import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class MinimalAuth extends ChangeNotifier {
  bool _signedIn = false;
  // final storage = FlutterSecureStorage();

  bool get signedIn => _signedIn;

  Future<void> signOut() async {
    await Future<void>.delayed(const Duration(milliseconds: 200));
    // Sign out.
    _signedIn = false;
    MinimalToaster.showToast("Амжилттай гарлаа");
    notifyListeners();
  }

  Future<bool> signIn(String username, String password) async {
    await Future<void>.delayed(const Duration(milliseconds: 200));
    // await storage.write(key: 'username', value: username);
    // await storage.write(key: 'password', value: password);
    // Sign in. Allow any password.
    _signedIn = true;
    notifyListeners();
    MinimalToaster.showToast("Амжилттай нэвтэрлээ");
    return _signedIn;
  }

  @override
  bool operator ==(Object other) =>
      other is MinimalAuth && other._signedIn == _signedIn;

  @override
  int get hashCode => _signedIn.hashCode;
}

class MinimalAuthScope extends InheritedNotifier<MinimalAuth> {
  const MinimalAuthScope({
    required super.notifier,
    required super.child,
    super.key,
  });

  static MinimalAuth of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<MinimalAuthScope>()!.notifier!;
}
